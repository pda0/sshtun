(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
unit ConThread;

interface

uses
  System.Classes, Winapi.Windows, Vcl.StdCtrls;

type
  TConThread = class(TThread)
  private
    FConMemo: TMemo;
    FMergeString: string;
    FMergeLine: Boolean;
    procedure SyncConsole;
  protected
    procedure ResetMergeLine; inline;
    procedure WriteLine(MsgText: string);
    procedure AppendLine(Msgtext: string);
    function SoftWin32Check(Source: string; RetVal: BOOL): BOOL; virtual;
    property ConMemo: TMemo read FConMemo;
  public
    constructor Create(const AConMemo: TMemo; CreateSuspended: Boolean = False);
  end;

implementation

uses
  System.SysUtils, Winapi.Messages, System.SysConst;

{ TConThread }

constructor TConThread.Create(const AConMemo: TMemo; CreateSuspended: Boolean);
begin
  FConMemo := AConMemo;
  FMergeString := '';
  ResetMergeLine;

  inherited Create(CreateSuspended);
end;

procedure TConThread.ResetMergeLine;
begin
  FMergeLine := False;
end;

procedure TConThread.SyncConsole;
var
  HasCrLf: Boolean;
begin
  HasCrLf := FMergeString.EndsWith(#13#10);
  if HasCrLf then
    FMergeString := FMergeString.Substring(0, FMergeString.Length - 2);

  if not FMergeLine or (ConMemo.Lines.Count = 0) then
    ConMemo.Lines.Add(FMergeString)
  else
    ConMemo.Lines[ConMemo.Lines.Count - 1] := ConMemo.Lines[ConMemo.Lines.Count - 1] + FMergeString;

  SendMessage(ConMemo.Handle, EM_LINESCROLL, 0, ConMemo.Lines.Count);

  FMergeLine := FMergeLine or (not HasCrLf);
end;

procedure TConThread.WriteLine(MsgText: string);
begin
  FMergeString := MsgText;
  ResetMergeLine;
  Synchronize(SyncConsole);
  ResetMergeLine;
end;

procedure TConThread.AppendLine(Msgtext: string);
begin
  FMergeString := Msgtext;
  Synchronize(SyncConsole);
end;

function TConThread.SoftWin32Check(Source: string; RetVal: BOOL): BOOL;
var
  LastError: DWORD;
begin
  if not RetVal then
  begin
    LastError := GetLastError;
    if LastError <> 0 then
      WriteLine(Source + ' ' + Format(SOSError, [LastError, SysErrorMessage(LastError), '']))
    else
      WriteLine(Source + ' ' + SUnkOSError + '.');
  end;
  Result := RetVal;
end;

end.
