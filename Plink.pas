(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
unit Plink;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  System.Classes, Winapi.Windows, Vcl.StdCtrls, ConThread, Settings;

type
  TSshStatus = (Connected, Disconnected, Failed);

  TSshNotifyEvent = procedure(Sender: TObject; SshStatus: TSshStatus) of object;

  TPlinkCtl = class(TConThread)
  private const
    EXIT_CODE_OK = 0;
    EXIT_CODE_SIGINT = 3221225786;
    EVENT_STOP = 0;
    EVENT_OUTPUT = 1;
    EVENT_PLINK = 2;
    EVENTS_AMOUNT = 3;
  private
    FEvents: TWOHandleArray;
    FPlinkPipe: THandle;
    FConOverlapped: TOverlapped;
    FSecAttr: TSecurityAttributes;
    FSettings: TSettingsForm;
    FCommandLine: string;
    FBuffer, FConvertedBuffer: TArray<Byte>;
    FBufferStartPos: Integer;
    FPlinkFailed: Boolean;
    FSshNotify: TSshNotifyEvent;
    procedure ResetOverlapped;
    function GetClientPipe: THandle;
    procedure CloseCilentPipe;
    function RunPageant: Boolean;
    function RunPlink: Boolean;
    procedure StopPlink;
    procedure SyncConnected;
    procedure SyncDisconnected;
    procedure SyncFailed;
    procedure CopyConsole(DataLen: Integer);
    function GetLine(var StartPos: Integer; DataLen: Integer; StripCrLf:
      Boolean = False): Integer;
  protected
    function SoftWin32Check(Source: string; RetVal: BOOL): BOOL; override;
    procedure TerminatedSet; override;
    procedure Execute; override;
  public
    constructor Create(const Settings: TSettingsForm; const AConMemo: TMemo;
      const ASshNotify: TSshNotifyEvent);
    destructor Destroy; override;
  end;

resourcestring
  ePlinkNotFound = 'Plink.exe not found at "%s".';
  ePageantNotFound = 'Pageant.exe not found at "%s".';

implementation

uses
  System.SysUtils, Vcl.Forms, Main;

{$IF NOT DEFINED(PIPE_REJECT_REMOTE_CLIENTS)}
const
  PIPE_REJECT_REMOTE_CLIENTS = $8;
{$ENDIF}

resourcestring
  sConnectionClosed = 'Connection closed.';
  ePlinkTerminated = 'Plink.exe terminated unexpectedly.';
  ePlinkClosePipe = 'Plink.exe channel unexpectedly closed.';

{ TPlinkCtl }

constructor TPlinkCtl.Create(const Settings: TSettingsForm; const AConMemo:
  TMemo; const ASshNotify: TSshNotifyEvent);
begin
  FreeOnTerminate := True;
  FSshNotify := ASshNotify;

  FBufferStartPos := 0;
  FillChar(FEvents[0], Length(FEvents) * SizeOf(FEvents[0]), 0);
  FEvents[EVENT_PLINK] := INVALID_HANDLE_VALUE;
  FEvents[EVENT_OUTPUT] := INVALID_HANDLE_VALUE;
  FPlinkPipe := INVALID_HANDLE_VALUE;
  FPlinkFailed := False;

  FSettings := Settings;

  FSecAttr.nLength := SizeOf(TSecurityAttributes);
  FSecAttr.bInheritHandle := True;
  FSecAttr.lpSecurityDescriptor := nil;

  FEvents[EVENT_STOP] := CreateEvent(nil, True, False, nil);
  SoftWin32Check('TPlinkCtl.Create::CreateEvent', FEvents[EVENT_STOP] <> 0);

  FCommandLine := (FSettings.PuTTYPath + 'plink.exe').QuotedString('"') +
    ' -ssh -batch -v -N -D localhost:' + FSettings.LocalPort.ToString + ' ' +
    FSettings.Login + '@' + FSettings.Host;

  if FSettings.Port <> FSettings.DEFAULT_SSH_PORT then
    FCommandLine := FCommandLine + ' -P ' + FSettings.Port.ToString;

  case FSettings.AuthType of
    TAuthType.Key:
      FCommandLine := FCommandLine + ' -i ' + FSettings.AuthText.QuotedString('"');
    TAuthType.Password:
      FCommandLine := FCommandLine + ' -pw ' + FSettings.AuthText.QuotedString('"');
  end;

  SetLength(FBuffer, 65536);
  SetLength(FConvertedBuffer, Length(FBuffer));

  if not Terminated then
    inherited Create(AConMemo);
end;

destructor TPlinkCtl.Destroy;
begin
  StopPlink;
  if FEvents[EVENT_STOP] <> 0 then
    CloseHandle(FEvents[EVENT_STOP]);

  inherited;
end;

procedure TPlinkCtl.TerminatedSet;
begin
  inherited;

  if FEvents[EVENT_STOP] <> 0 then
    SetEvent(FEvents[EVENT_STOP]);
end;

procedure TPlinkCtl.SyncConnected;
begin
  if Assigned(FSshNotify) then
    FSshNotify(Self, TSshStatus.Connected);
end;

procedure TPlinkCtl.SyncDisconnected;
begin
  if Assigned(FSshNotify) then
    FSshNotify(Self, TSshStatus.Disconnected);
end;

procedure TPlinkCtl.SyncFailed;
begin
  if Assigned(FSshNotify) then
    FSshNotify(Self, TSshStatus.Failed);
end;

function TPlinkCtl.SoftWin32Check(Source: string; RetVal: BOOL): BOOL;
begin
  Result := inherited;
  if not Result then
  begin
    FPlinkFailed := True;
    if GetCurrentThreadId = MainThreadID then
      SyncFailed;
    Terminate;
  end;
end;

procedure TPlinkCtl.ResetOverlapped;
begin
  FillChar(FConOverlapped, SizeOf(FConOverlapped), 0);
  FConOverlapped.hEvent := FEvents[EVENT_OUTPUT];
  ResetEvent(FEvents[EVENT_OUTPUT]);
  FBufferStartPos := 0;
end;

function TPlinkCtl.GetClientPipe: THandle;
var
  PipeName: string;
  PipeMode, WaitResult, LastError: DWORD;
begin
  CloseCilentPipe;

  PipeName := '\\.\pipe\LOCAL\sshtun-' + TGUID.NewGuid.ToString.Substring(1, 36).ToLower;
  FSecAttr.nLength := SizeOf(TSecurityAttributes);
  FSecAttr.bInheritHandle := True;
  FSecAttr.lpSecurityDescriptor := nil;

  PipeMode := PIPE_TYPE_BYTE or PIPE_READMODE_BYTE or PIPE_WAIT;
  if CheckWin32Version(6, 0) then
      PipeMode := PipeMode or PIPE_REJECT_REMOTE_CLIENTS;

  FPlinkPipe := CreateNamedPipe(
    PChar(PipeName),
    PIPE_ACCESS_INBOUND or FILE_FLAG_OVERLAPPED or FILE_FLAG_FIRST_PIPE_INSTANCE,
    PipeMode,
    1,
    0,
    0,
    NMPWAIT_USE_DEFAULT_WAIT,
    nil
  );
  if not SoftWin32Check('TPlinkCtl.GetClientPipe::CreateNamedPipe',
    FPlinkPipe <> INVALID_HANDLE_VALUE) then
  begin
    Exit(INVALID_HANDLE_VALUE);
  end;

  FEvents[EVENT_OUTPUT] := CreateEvent(nil, True, False, nil);
  if SoftWin32Check('TPlinkCtl.GetClientPipe::CreateEvent',
    FEvents[EVENT_OUTPUT] <> 0) then
    ResetOverlapped
  else begin
    FEvents[EVENT_OUTPUT] := INVALID_HANDLE_VALUE;
    CloseCilentPipe;
    Exit(INVALID_HANDLE_VALUE);
  end;

  if not ConnectNamedPipe(FPlinkPipe, @FConOverlapped) then
  begin
    LastError := GetLastError;
    if LastError <> ERROR_IO_PENDING then
    begin
      SetLastError(LastError);
      SoftWin32Check('TPlinkCtl.GetClientPipe::ConnectNamedPipe', False);
      CloseCilentPipe;
      Exit(INVALID_HANDLE_VALUE);
    end;
  end;

  Result := CreateFile(
    PChar(PipeName),
    GENERIC_WRITE or FILE_WRITE_DATA or WinApi.Windows.SYNCHRONIZE,
    0,
    @FSecAttr,
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL or FILE_FLAG_WRITE_THROUGH or FILE_FLAG_NO_BUFFERING,
    0
  );
  if not SoftWin32Check('TPlinkCtl.GetClientPipe::CreateFile',
    Result <> INVALID_HANDLE_VALUE) then
  begin
    CloseCilentPipe;
    Exit(INVALID_HANDLE_VALUE);
  end;

  WaitResult := WaitForMultipleObjects(2, @FEvents, False, INFINITE);
  case WaitResult of
    WAIT_OBJECT_0..(WAIT_OBJECT_0 + 1): begin
      case WaitResult - WAIT_OBJECT_0 of
        EVENT_OUTPUT: begin
          if not SoftWin32Check('TPlinkCtl.GetClientPipe::GetOverlappedResult',
            GetOverlappedResult(FPlinkPipe, FConOverlapped, PipeMode, True)) then
          begin
            CloseCilentPipe;
            CloseHandle(Result);
            Result := INVALID_HANDLE_VALUE;
          end;
        end;
        EVENT_STOP: begin
          { ���� �� ��������� ����� - ������������ ����� �� �������� }
          CloseCilentPipe;
          CloseHandle(Result);
          Result := INVALID_HANDLE_VALUE;
        end;
        else begin
          SoftWin32Check('TPlinkCtl.GetClientPipe::WaitForMultipleObjects', False);
          CloseCilentPipe;
          CloseHandle(Result);
          Result := INVALID_HANDLE_VALUE;
        end;
      end;
    end;
    else begin
      SoftWin32Check('TPlinkCtl.GetClientPipe::WaitForMultipleObjects', False);
      CloseCilentPipe;
      CloseHandle(Result);
      Result := INVALID_HANDLE_VALUE;
    end;
  end;
end;

procedure TPlinkCtl.CloseCilentPipe;
begin
  if FEvents[EVENT_OUTPUT] <> INVALID_HANDLE_VALUE then
  begin
    CloseHandle(FEvents[EVENT_OUTPUT]);
    FEvents[EVENT_OUTPUT] := INVALID_HANDLE_VALUE;
  end;

  if FPlinkPipe <> INVALID_HANDLE_VALUE then
  begin
    CloseHandle(FPlinkPipe);
    FPlinkPipe := INVALID_HANDLE_VALUE;
  end;
end;

function TPlinkCtl.RunPageant: Boolean;
var
  StartupInfo: TStartupInfo;
  ProcessInformation: TProcessInformation;
  PageantCommandLine: string;
begin
  if FSettings.AuthText.IsEmpty then
  begin
    { ��� �� �� ������� ���� � pageant, �� ���� ��������� �� ������� �� ��,
      � �� �� ���������. }
    if FindWindow(PChar('Pageant'), PChar('Pageant')) <> 0 then
      { ��� ������� }
      Exit(True);

    PageantCommandLine := (FSettings.PuTTYPath + 'pageant.exe').QuotedString('"');
  end
  else
    PageantCommandLine := (FSettings.PuTTYPath + 'pageant.exe').QuotedString('"') +
      ' ' + FSettings.AuthText.QuotedString('"');

  if not FileExists(FSettings.PuTTYPath + 'pageant.exe') then
  begin
    WriteLine(MainForm.Langs[ePlinkNotFound]);
    Exit(False);
  end;

  FillChar(StartupInfo, SizeOf(TStartupInfo), 0);
  FillChar(ProcessInformation, SizeOf(TProcessInformation), 0);
  StartupInfo.cb := SizeOf(TStartupInfo);

  Result := SoftWin32Check('TPlinkCtl.RunPlink::CreateProcess',
    CreateProcess(
      nil,
      PChar(PageantCommandLine),
      nil,
      nil,
      True,
      NORMAL_PRIORITY_CLASS,
      nil,
      nil,
      StartupInfo,
      ProcessInformation
    )
  );
  if Result then
  begin
    CloseHandle(ProcessInformation.hThread);
    CloseHandle(ProcessInformation.hProcess);
  end;
end;

function TPlinkCtl.RunPlink: Boolean;
var
  StartupInfo: TStartupInfo;
  ProcessInformation: TProcessInformation;
begin
  if not FileExists(FSettings.PuTTYPath + 'plink.exe') then
  begin
    WriteLine(MainForm.Langs[ePlinkNotFound]);
    Exit(False);
  end;
  if FSettings.AuthType = TAuthType.Pageant then
    if not RunPageant then
      Exit(False);

  FillChar(StartupInfo, SizeOf(TStartupInfo), 0);
  FillChar(ProcessInformation, SizeOf(TProcessInformation), 0);
  StartupInfo.cb := SizeOf(TStartupInfo);

  StartupInfo.hStdOutput := GetClientPipe;
  StartupInfo.hStdError := StartupInfo.hStdOutput;
  if StartupInfo.hStdOutput = INVALID_HANDLE_VALUE then
    Exit(False);

  StartupInfo.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
  StartupInfo.wShowWindow := SW_HIDE;

  Result := SoftWin32Check('TPlinkCtl.RunPlink::CreateProcess',
    CreateProcess(
      nil,
      PChar(FCommandLine),
      @FSecAttr,
      @FSecAttr,
      True,
      NORMAL_PRIORITY_CLASS or CREATE_NEW_CONSOLE or WinApi.Windows.SYNCHRONIZE,
      nil,
      nil,
      StartupInfo,
      ProcessInformation
    )
  );
  if Result then
  begin
    ResetMergeLine;
    FEvents[EVENT_PLINK] := ProcessInformation.hProcess;
    CloseHandle(ProcessInformation.hThread);
  end
  else begin
    FEvents[EVENT_PLINK] := INVALID_HANDLE_VALUE;
    CloseCilentPipe;
  end;
end;

procedure TPlinkCtl.StopPlink;
begin
  CloseCilentPipe;

  if FEvents[EVENT_PLINK] <> INVALID_HANDLE_VALUE then
  begin
    TerminateProcess(FEvents[EVENT_PLINK], 0);
    CloseHandle(FEvents[EVENT_PLINK]);
    FEvents[EVENT_PLINK] := INVALID_HANDLE_VALUE;
  end;
end;

function TPlinkCtl.GetLine(var StartPos: Integer; DataLen: Integer;
  StripCrLf: Boolean): Integer;
var
  i, CrPos: DWORD;
begin
  if StartPos < (DataLen - 1) then
  begin
    CrPos := DataLen - 1;
    for i := StartPos to DataLen - 1 do
      if FConvertedBuffer[i] = 10 then
      begin
        CrPos := i;
        Break;
      end;

    if StripCrLf then
      Result := Integer(CrPos) - Integer(StartPos) - 1
    else
      Result := Integer(CrPos) - Integer(StartPos) + 1;
    StartPos := CrPos + 1;
  end
  else
    Result := 0;
end;

procedure TPlinkCtl.CopyConsole(DataLen: Integer);
var
  MergeString: string;
  CopyPos, StrLen: Integer;
begin
  DataLen := DataLen + FBufferStartPos;
  FBuffer[DataLen] := 0;
  OemToCharA(@FBuffer[FBufferStartPos], @FConvertedBuffer[FBufferStartPos]);

  CopyPos := FBufferStartPos;
  StrLen := GetLine(FBufferStartPos, DataLen);
  while StrLen > 0 do
  begin
    MergeString := TEncoding.ANSI.GetString(FConvertedBuffer, CopyPos, StrLen);
    AppendLine(MergeString);
    if MergeString.Contains('SOCKS dynamic forwarding') then
      Synchronize(SyncConnected);

    CopyPos := FBufferStartPos;
    StrLen := GetLine(FBufferStartPos, DataLen);
  end;
end;

procedure TPlinkCtl.Execute;
var
  WaitResult, LastError, ExitCode: DWORD;
  Transferred: Integer;
  Pending: Boolean;
begin
  Pending := False;

  while not Terminated do
  begin
    if FEvents[EVENT_PLINK] = INVALID_HANDLE_VALUE then
      if not RunPlink then
        Break;

    WaitResult := WaitForMultipleObjects(EVENTS_AMOUNT, @FEvents, False, INFINITE);
    case WaitResult of
      WAIT_OBJECT_0..(WAIT_OBJECT_0 + EVENTS_AMOUNT - 1):
      begin
        case WaitResult - WAIT_OBJECT_0 of
          EVENT_OUTPUT: begin
            Transferred := 0;
            SetLastError(ERROR_SUCCESS);

            if Pending then
            begin
              if GetOverlappedResult(FPlinkPipe, FConOverlapped,
                DWORD(Transferred), False) then
              begin
                Pending := False;
                if Transferred > 0 then
                  CopyConsole(Transferred);
              end
              else begin
                LastError := GetLastError;
                ResetEvent(FEvents[EVENT_OUTPUT]);
                case LastError of
                  ERROR_IO_INCOMPLETE:
                    if Transferred > 0 then
                      CopyConsole(Transferred)
                    else begin
                      SetLastError(LastError);
                      SoftWin32Check('TPlinkCtl.Execute::GetOverlappedResult', False);
                    end;
                  ERROR_HANDLE_EOF: begin
                    { ������ ������ ����� �� ���������� ������, ����
                      ������������� ���, �.�. ������ ������ �� ����� }
                    WriteLine(MainForm.Langs[ePlinkClosePipe]);
                    StopPlink;
                  end;
                  else begin
                    { ������ ������� }
                    SetLastError(LastError);
                    SoftWin32Check('TPlinkCtl.Execute::GetOverlappedResult', False);
                  end;
                end;
              end;
            end
            else begin
              ResetOverlapped;
              if ReadFile(FPlinkPipe, FBuffer[FBufferStartPos], Length(FBuffer) - 1,
                DWORD(Transferred), @FConOverlapped) then
                CopyConsole(Transferred)
              else begin
                LastError := GetLastError;
                case LastError of
                  ERROR_IO_PENDING:
                    Pending := True;
                  else begin
                    { ������ ������� }
                    SetLastError(LastError);
                    SoftWin32Check('TPlinkCtl.Execute::ReadFile', False);
                  end;
                end;
              end;
            end;
          end;
          EVENT_PLINK: begin
            if SoftWin32Check('TPlinkCtl.Execute::GetExitCodeProcess',
              GetExitCodeProcess(FEvents[EVENT_PLINK], ExitCode)) then
              if (ExitCode = EXIT_CODE_OK) or (ExitCode = EXIT_CODE_SIGINT) then
                { Plink ���� ��� ������ ����������, ���� ������������� ��� }
                WriteLine(MainForm.Langs[ePlinkTerminated])
              else begin
                { Plink ���������� � �������, ������� ������ � ����������
                  ������������ }
                FPlinkFailed := True;
                Terminate;
              end;

            CloseHandle(FEvents[EVENT_PLINK]);
            FEvents[EVENT_PLINK] := INVALID_HANDLE_VALUE;
          end;
          EVENT_STOP: begin
            WriteLine(MainForm.Langs[sConnectionClosed]);
            Break;
          end;
        end;
      end;
      WAIT_ABANDONED_0..(WAIT_ABANDONED_0 + EVENTS_AMOUNT - 1):
        case WaitResult - WAIT_ABANDONED_0 of
          EVENT_OUTPUT, EVENT_PLINK:
            if GetExitCodeProcess(FEvents[EVENT_PLINK], ExitCode) then
            begin
              if (ExitCode = EXIT_CODE_OK) or (ExitCode = EXIT_CODE_SIGINT) then
                { Plink ���� ��� ������ ����������, ���� ������������� ��� }
                WriteLine(MainForm.Langs[ePlinkTerminated])
              else begin
                { Plink ���������� � �������, ������� ������ � ����������
                  ������������ }
                FPlinkFailed := True;
                Terminate;
              end;

              CloseHandle(FEvents[EVENT_PLINK]);
              FEvents[EVENT_PLINK] := INVALID_HANDLE_VALUE;
              CloseCilentPipe;
            end
            else begin
              LastError := GetLastError;
              if LastError = STILL_ACTIVE then
              begin
                { � �������-�� ��� �������� }
                WriteLine(MainForm.Langs[ePlinkTerminated]);
                StopPlink;
              end
              else begin
                SetLastError(LastError);
                SoftWin32Check('TPlinkCtl.Execute::GetExitCodeProcess', False);
              end;
            end;
          EVENT_STOP:
            { � ��� ����������� ������� �� ������ ����������� ���� �� ���� }
            SoftWin32Check('TPlinkCtl.Execute::WaitForMultipleObjects', False);
        end;
      WAIT_TIMEOUT:
        Continue; { Wait, wat? o_O }
      else
        SoftWin32Check('TPlinkCtl.Execute::WaitForMultipleObjects', False);
    end;
  end;

  StopPlink;
  if FPlinkFailed then
    Synchronize(SyncFailed)
  else
   Synchronize(SyncDisconnected);
end;

end.
