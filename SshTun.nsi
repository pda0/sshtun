﻿;   SshTun, is a small utility that allows you to turn almost any ssh server
;   into a proxy server.
;   Copyright (C) 2018  Dmitriy Pomerantsev
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

Unicode true
XPStyle on
SetCompressor /SOLID lzma
RequestExecutionLevel admin

;--------------------------------
; Определения

!define PRODUCT_NAME "SshTun"
!include SshTun.nsh
!define PRODUCT_FOLDER ${PRODUCT_NAME}
!define PRODUCT_WEB_SITE "https://gitlab.com/pda0/sshtun"
!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"

!define PRODUCT_VERSION "${PRODUCT_VERSION_MAJOR}.${PRODUCT_VERSION_MINOR}.${PRODUCT_VERSION_PATCHLEVEL}${PRODUCT_VERSION_SUFFIX}"

;--------------------------------
; Включаемые файлы

!include "WinVer.nsh"
!include "MUI2.nsh"
!include "nsDialogs.nsh"
!include "x64.nsh"
!include "FileFunc.nsh"

; Нужные функции
!insertmacro GetParameters
!insertmacro GetOptions
!insertmacro un.GetParameters
!insertmacro un.GetOptions

;--------------------------------
; Общая секция

Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
OutFile "sshtun-setup-${PRODUCT_VERSION}.exe"
InstallDir $PROGRAMFILES\${PRODUCT_FOLDER}

;--------------------------------
; Определения MUI

var ICONS_GROUP

!define MUI_ABORTWARNING
!define MUI_CUSTOMFUNCTION_GUIINIT onGUIInit

!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"

!define MUI_LANGDLL_REGISTRY_ROOT HKLM
!define MUI_LANGDLL_REGISTRY_KEY "${PRODUCT_UNINST_KEY}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "NSIS:Language"

!define MUI_LICENSEPAGE_CHECKBOX

;--------------------------------
; Страницы

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "LICENSE"
Page custom PageReinstall PageLeaveReinstall
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_STARTMENU Application $ICONS_GROUP
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!define MUI_PAGE_CUSTOMFUNCTION_PRE un.ConfirmPagePre
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!define MUI_PAGE_CUSTOMFUNCTION_PRE un.FinishPagePre
!insertmacro MUI_UNPAGE_FINISH

;--------------------------------
; Языки

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Russian"

LangString TEXT_ADMIN_REQUIRES ${LANG_ENGLISH} "This installer requires admin privileges to run."
LangString TEXT_ADMIN_REQUIRES ${LANG_RUSSIAN} "Для запуска установщика необходимы права администратора."

LangString TEXT_INSTALLED_TITLE ${LANG_ENGLISH} "Program Maintenance"
LangString TEXT_INSTALLED_TITLE ${LANG_RUSSIAN} "Обслуживание программ"
LangString TEXT_INSTALLED_EXPLAIN ${LANG_ENGLISH} "Install or remove the program."
LangString TEXT_INSTALLED_EXPLAIN ${LANG_RUSSIAN} "Установка или удаление программы."
LangString TEXT_INSTALLED_INFO ${LANG_ENGLISH} "The %s is already installed. Select the operation you want to perform and click $\"Next$\" to continue."
LangString TEXT_INSTALLED_INFO ${LANG_RUSSIAN} "%s уже установлен. Выберите тип операции и нажмите кнопку $\"Далее$\" для продолжения."

LangString TEXT_INSTALLED_REINSTALL ${LANG_ENGLISH} "Reinstall the program."
LangString TEXT_INSTALLED_REINSTALL ${LANG_RUSSIAN} "Переустановить программу."
LangString TEXT_INSTALLED_REMOVE ${LANG_ENGLISH} "Remove the %s."
LangString TEXT_INSTALLED_REMOVE ${LANG_RUSSIAN} "Удалить %s."

;--------------------------------
; Секция установки

Section
  SetShellVarContext all

  SetOutPath $INSTDIR
  ${If} ${RunningX64}
    File /r "Win64\Release\${PRODUCT_NAME}.exe"
  ${Else}
    File /r "Win32\Release\${PRODUCT_NAME}.exe"
  ${EndIf}
  File "${PRODUCT_NAME}.rus"
  File "LICENSE"

  ; Ярлыки
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
  CreateDirectory "$SMPROGRAMS\$ICONS_GROUP"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_NAME}.exe"
  WriteIniStr "$INSTDIR\${PRODUCT_NAME}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Website.lnk" "$INSTDIR\${PRODUCT_NAME}.url"
  CreateShortCut "$SMPROGRAMS\$ICONS_GROUP\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  !insertmacro MUI_STARTMENU_WRITE_END
SectionEnd

Section -Prerequisites
  ClearErrors
  ${If} ${RunningX64}
    EnumRegKey $0 HKLM "SOFTWARE\SimonTatham\PuTTY64" ""
  ${Else}
    EnumRegKey $0 HKLM "SOFTWARE\SimonTatham\PuTTY" ""
  ${EndIf}
  IfErrors 0 PuTTYInstalled
	${If} ${RunningX64}
	  File "putty-64bit-0.70-installer.msi"
	  ExecWait '"$SYSDIR\msiexec.exe" /i "$INSTDIR\putty-64bit-0.70-installer.msi"'
	${Else}
	  File "putty-0.70-installer.msi"
	  ExecWait '"$SYSDIR\msiexec.exe" /i "$INSTDIR\putty-0.70-installer.msi"'
	${EndIf}
  PuTTYInstalled:
  Delete "$INSTDIR\putty-0.70-installer.msi"
  Delete "$INSTDIR\putty-64bit-0.70-installer.msi"
SectionEnd

Section -Post
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayName" "${PRODUCT_NAME}"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "UninstallString" "$\"$INSTDIR\Uninstall.exe$\""
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "QuietUninstallString" "$\"$INSTDIR\Uninstall.exe$\" /S"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "InstallLocation" "$\"$INSTDIR$\""
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\${PRODUCT_NAME}.exe"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"
  WriteRegDWORD HKLM "${PRODUCT_UNINST_KEY}" "VersionMajor" ${PRODUCT_VERSION_MAJOR}
  WriteRegDWORD HKLM "${PRODUCT_UNINST_KEY}" "VersionMinor" ${PRODUCT_VERSION_MINOR}
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"
  WriteRegDWORD HKLM "${PRODUCT_UNINST_KEY}" "NoModify" 1
  WriteRegDWORD HKLM "${PRODUCT_UNINST_KEY}" "NoRepair" 1
SectionEnd

;--------------------------------
; Секция удаления

Section "Uninstall"
  !insertmacro MUI_STARTMENU_GETFOLDER "Application" $ICONS_GROUP
  Delete "$SMPROGRAMS\$ICONS_GROUP\Website.lnk"
  Delete "$SMPROGRAMS\$ICONS_GROUP\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$ICONS_GROUP"

  Delete "$INSTDIR\${PRODUCT_NAME}.exe"
  Delete "$INSTDIR\${PRODUCT_NAME}.rus"
  Delete "$INSTDIR\${PRODUCT_NAME}.url"
  Delete "$INSTDIR\LICENSE"
  Delete "$INSTDIR\Uninstall.exe"
  Delete "$INSTDIR\putty-64bit-0.70-installer.msi"
  Delete "$INSTDIR\putty-0.70-installer.msi"
  RMDir "$INSTDIR"

  DeleteRegKey HKLM "${PRODUCT_UNINST_KEY}"

  SetAutoClose true
SectionEnd

;--------------------------------
; Секция функций и макросов

Var PREVIOUS_VERSION
Var PREVIOUS_UNINSTALLER
Var PREVIOUS_UNINSTALLER_SILENT
Var PREVIOUS_CHOICE
Var PREVIOUS_REINSTALL_BUTTON
Var PREVIOUS_UNINSTALL_BUTTON

Function .onInit
  ${If} ${RunningX64}
    ${EnableX64FSRedirection}
    SetRegView 64
    StrCpy "$INSTDIR" "$PROGRAMFILES64\${PRODUCT_FOLDER}"
  ${Else}
    ${DisableX64FSRedirection}
    SetRegView 32
    StrCpy "$INSTDIR" "$PROGRAMFILES64\${PRODUCT_FOLDER}"
  ${EndIf}
  
  InitPluginsDir
  
  StrCpy $ICONS_GROUP "${PRODUCT_NAME}"
  
  ; Устанавливаем значения переменных
  Push 0
  Pop $PREVIOUS_CHOICE
  
  ; Диалог выбора языка
  !insertmacro MUI_LANGDLL_DISPLAY
  
  ; Читаем информацию о версии установленного каталога (проверяя его наличие)
  Call ReadPreviousVersion
  
  BringToFront
FunctionEnd

Function un.onInit
  ${If} ${RunningX64}
    ${EnableX64FSRedirection}
    SetRegView 64
  ${Else}
    ${DisableX64FSRedirection}
    SetRegView 32
  ${EndIf}
  
  BringToFront
FunctionEnd

Function onGUIInit
  ; Проверяем версию системы и права.
  ; В Windows Vista можно задать в манифесте требования прав администратора.
  ${If} ${AtMostWinXP}
    ClearErrors
    UserInfo::getAccountType
    pop $0
    ${If} $0 != "Admin"
      MessageBox MB_OK|MB_ICONSTOP "$(TEXT_ADMIN_REQUIRES)"
	  setErrorLevel 740 ;ERROR_ELEVATION_REQUIRED
      Abort
    ${EndIf}
  ${EndIf}
FunctionEnd

Function ReadPreviousVersion
  ReadRegStr $PREVIOUS_UNINSTALLER HKLM "${PRODUCT_UNINST_KEY}" "UninstallString"
  ${If} $PREVIOUS_UNINSTALLER != ""
	ReadRegStr $PREVIOUS_UNINSTALLER_SILENT HKLM "${PRODUCT_UNINST_KEY}" "QuietUninstallString"
	${If} $PREVIOUS_UNINSTALLER_SILENT == ""
	  StrCpy $PREVIOUS_UNINSTALLER_SILENT "$PREVIOUS_UNINSTALLER /S"
	${EndIf}
	ReadRegStr $PREVIOUS_VERSION HKLM "${PRODUCT_UNINST_KEY}" "DisplayVersion"
  ${EndIf}
FunctionEnd

Function RadioClick
  Pop $R0
  ${If} $R0 == $PREVIOUS_REINSTALL_BUTTON
    Push 1
  ${ElseIf} $R0 == $PREVIOUS_UNINSTALL_BUTTON
    Push 2
  ${EndIf}
  Pop $PREVIOUS_CHOICE
  
  ; Активируем кнопку "Next"
  GetDlgItem $R0 $HWNDPARENT 1
  EnableWindow $R0 1
FunctionEnd

Function StrReplace
  ; http://nsis.sourceforge.net/mediawiki/index.php?title=StrReplace&oldid=4108
  Exch $0 ;this will replace wrong characters
  Exch
  Exch $1 ;needs to be replaced
  Exch
  Exch 2
  Exch $2 ;the original string
  Push $3 ;counter
  Push $4 ;temp character
  Push $5 ;temp string
  Push $6 ;length of string that need to be replaced
  Push $7 ;length of string that will replace
  Push $R0 ;tempstring
  Push $R1 ;tempstring
  Push $R2 ;tempstring
  StrCpy $3 "-1"
  StrCpy $5 ""
  StrLen $6 $1
  StrLen $7 $0
  Loop:
  IntOp $3 $3 + 1
  Loop_noinc:
  StrCpy $4 $2 $6 $3
  StrCmp $4 "" ExitLoop
  StrCmp $4 $1 Replace
  Goto Loop
  Replace:
  StrCpy $R0 $2 $3
  IntOp $R2 $3 + $6
  StrCpy $R1 $2 "" $R2
  StrCpy $2 $R0$0$R1
  IntOp $3 $3 + $7
  Goto Loop_noinc
  ExitLoop:
  StrCpy $0 $2
  Pop $R2
  Pop $R1
  Pop $R0
  Pop $7
  Pop $6
  Pop $5
  Pop $4
  Pop $3
  Pop $2
  Pop $1
  Exch $0
FunctionEnd

Function PageReinstall
  ${If} $PREVIOUS_UNINSTALLER == ""
    Abort
  ${EndIf}
  
  !insertmacro MUI_HEADER_TEXT $(TEXT_INSTALLED_TITLE) $(TEXT_INSTALLED_EXPLAIN)
  
  ; Создаём диалог
  nsDialogs::Create 1018
  Pop $R0
  ${If} $R0 == error
    Abort
  ${EndIf}
  
  Push "$(TEXT_INSTALLED_INFO)"
  Push "%s"
  Push "${PRODUCT_NAME} $PREVIOUS_VERSION"
  Call StrReplace
  Pop $R0
  ${NSD_CreateLabel} 0 0 100% 24u $R0
  Pop $R0
  
  ${NSD_CreateRadioButton} 10% 40% 40% 12u $(TEXT_INSTALLED_REINSTALL)
  Pop $PREVIOUS_REINSTALL_BUTTON
  ${NSD_AddStyle} $PREVIOUS_REINSTALL_BUTTON ${WS_GROUP}
  ${NSD_OnClick} $PREVIOUS_REINSTALL_BUTTON RadioClick
  
  Push "$(TEXT_INSTALLED_REMOVE)"
  Push "%s"
  Push "${PRODUCT_NAME}"
  Call StrReplace
  Pop $R0
  ${NSD_CreateRadioButton} 10% 52% 40% 12u $R0
  Pop $PREVIOUS_UNINSTALL_BUTTON
  ${NSD_OnClick} $PREVIOUS_UNINSTALL_BUTTON RadioClick
  
  ; Проверяем - надо ли отключить кнопку "Next"
  GetDlgItem $R0 $HWNDPARENT 1
  ${If} $PREVIOUS_CHOICE > 0
    ${If} $PREVIOUS_CHOICE == 1
	  SendMessage $PREVIOUS_REINSTALL_BUTTON ${BM_SETCHECK} ${BST_CHECKED} 0
	${Else}
	  SendMessage $PREVIOUS_UNINSTALL_BUTTON ${BM_SETCHECK} ${BST_CHECKED} 0
	${EndIf}
	Push 1
  ${Else}
    Push 0
  ${EndIf}
  Pop $R1
  EnableWindow $R0 $R1
  
  nsDialogs::Show
FunctionEnd

Function RunUninstaller
  ; Запускаем деинсталляцию
  HideWindow
  ClearErrors
  
  ${If} $PREVIOUS_CHOICE == 1
    ; Переустановить
    ExecWait '$PREVIOUS_UNINSTALLER_SILENT _?=$INSTDIR'
  ${Else}
    ; Удалить
	ExecWait '$PREVIOUS_UNINSTALLER _?=$INSTDIR'
  ${EndIf}
  
  IfErrors no_remove_uninstaller
  IfFileExists "$INSTDIR\Uninstall.exe" 0 no_remove_uninstaller
  
  Delete "$PREVIOUS_UNINSTALLER"
  RMDir "$INSTDIR"
  
no_remove_uninstaller:
  ${If} $PREVIOUS_CHOICE == 1
    ; Переустановить
    BringToFront
  ${ElseIf} $PREVIOUS_CHOICE == 2
    ; Удалить
	Quit
  ${EndIf}
FunctionEnd

Function PageLeaveReinstall
  ${If} $PREVIOUS_CHOICE > 0
    Call RunUninstaller
  ${EndIf}
FunctionEnd

Function un.ConfirmPagePre
  ${un.GetParameters} $R0
  
  ${un.GetOptions} $R0 "/S" $R1
  ${Unless} ${Errors}
    Abort
  ${EndUnless}
FunctionEnd
 
Function un.FinishPagePre
  ${un.GetParameters} $R0
  
  ${un.GetOptions} $R0 "/S" $R1
  ${Unless} ${Errors}
    SetRebootFlag false
    Abort
  ${EndUnless}
FunctionEnd
