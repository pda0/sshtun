(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
unit UpdateChecker;

interface

uses
  System.Classes, Winapi.Windows, Vcl.StdCtrls, ConThread, Main;

type
  TVersionInfo = record
  private
    FSuffix: string;
    FMajor, FMinor, FPatchLevel, FBuild: Word;
    FReaded: Boolean;
  public
    constructor Create(ExeName: string);
    property Major: Word read FMajor;
    property Minor: Word read FMinor;
    property PatchLevel: Word read FPatchLevel;
    property Build: Word read FBuild;
    property Suffix: string read FSuffix;
    property InfoReaded: Boolean read FReaded;
  end;

  TUpdateThread = class(TConThread)
  private
    FMainForm: TMainForm;
    FNewVersion: string;
    FTermEvent: THandle;
    FOnDestroy: TNotifyEvent;
    procedure DoFinalize;
    procedure NotifyUpdate;
    procedure NotifyUpdateComplete;
    function CompareVersion(LastRelease: string): Boolean;
    function CheckLatestRelease(const Response: string): Boolean;
  protected
    function SoftWin32Check(Source: string; RetVal: BOOL): BOOL; override;
    procedure TerminatedSet; override;
    procedure Execute; override;
  public
    constructor Create(const AMainForm: TMainForm; const AConMemo: TMemo;
      OnDestroy: TNotifyEvent);
    destructor Destroy; override;
  end;

const
  PROJECT_ID = 'pda0/sshtun';
  PROJECT_HOST: string  = 'gitlab.com';
  PROJECT_RELEASES = 'api/v4/projects/%s/repository/tags';
  URL_RELEASES = 'https://%s/%s/tags';

var
  VersionInfo: TVersionInfo;

implementation

uses
  System.SysUtils, Winapi.WinHTTP, System.NetEncoding, System.JSON, Vcl.Forms,
  System.ZLib;

resourcestring
  eUpdateFailed = 'Failed to check for updates.';

{$IFNDEF WINHTTP_OPTION_DECOMPRESSION}
const
  WINHTTP_OPTION_DECOMPRESSION = 118;
  {$EXTERNALSYM WINHTTP_OPTION_DECOMPRESSION}
{$ENDIF}
{$IFNDEF WINHTTP_DECOMPRESSION_FLAG_GZIP}
const
  WINHTTP_DECOMPRESSION_FLAG_GZIP = $00000001;
  {$EXTERNALSYM WINHTTP_DECOMPRESSION_FLAG_GZIP}
{$ENDIF}
{$IFNDEF WINHTTP_DECOMPRESSION_FLAG_DEFLATE}
const
  WINHTTP_DECOMPRESSION_FLAG_DEFLATE = $00000002;
  {$EXTERNALSYM WINHTTP_DECOMPRESSION_FLAG_DEFLATE}
{$ENDIF}
{$IFNDEF WINHTTP_DECOMPRESSION_FLAG_ALL}
const
  WINHTTP_DECOMPRESSION_FLAG_ALL = WINHTTP_DECOMPRESSION_FLAG_GZIP or WINHTTP_DECOMPRESSION_FLAG_DEFLATE;
  {$EXTERNALSYM WINHTTP_DECOMPRESSION_FLAG_ALL}
{$ENDIF}

{ TVersionInfo }

{$WARN USE_BEFORE_DEF OFF}
constructor TVersionInfo.Create(ExeName: string);
type
  PVerLang = ^TVerLang;
  TVerLang = packed record
    LangId, CodePage: Word;
  end;
var
  VerInfoSize, VerValueSize, Dummy: DWORD;
  VerInfo: Pointer;
  VerValue: PVSFixedFileInfo;
  VerLang: PVerLang;
  VerComments: Pointer;
  LangInfo: string;
begin
  FReaded := True;
  FSuffix := '';

  VerInfoSize := GetFileVersionInfoSize(PChar(ExeName), Dummy);
  if VerInfoSize > 0 then
  begin
    GetMem(VerInfo, VerInfoSize);
    try
      if GetFileVersionInfo(PChar(ExeName), 0, VerInfoSize,
        VerInfo) then
      begin
        VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
        with VerValue^ do
        begin
          FMajor := dwFileVersionMS shr 16;
          FMinor := dwFileVersionMS and $FFFF;
          FPatchLevel := dwFileVersionLS shr 16;
          FBuild := dwFileVersionLS and $FFFF;
        end;

        VerQueryValue(VerInfo, '\VarFileInfo\Translation', Pointer(VerLang),
          VerValueSize);
        if VerValueSize < SizeOf(TVerLang) then
          FReaded := False
        else begin
          LangInfo := IntToHex(VerLang^.LangId, 4) + IntToHex(VerLang^.CodePage, 4);
          VerQueryValue(VerInfo, PChar('\StringFileInfo\' + LangInfo + '\Comments'),
            VerComments, VerValueSize);
          if VerValueSize > 0 then
          begin
            SetLength(FSuffix, VerValueSize - 1); { ����� #0 }
            Move(VerComments^, FSuffix[Low(FSuffix)], (VerValueSize - 1) * SizeOf(Char));
          end;
        end;
      end;
    finally
      FreeMem(VerInfo, VerInfoSize);
    end;
  end
  else
    FReaded := False;
end;
{$WARN USE_BEFORE_DEF DEFAULT}

{ TUpdateThread }

constructor TUpdateThread.Create(const AMainForm: TMainForm; const AConMemo:
  TMemo; OnDestroy: TNotifyEvent);
begin
  FMainForm := AMainForm;
  FOnDestroy := OnDestroy;
  FTermEvent := CreateEvent(nil, True, False, nil);
  SoftWin32Check('TUpdateThread.Create::CreateEvent', FTermEvent <> 0);
  FreeOnTerminate := True;

  inherited Create(AConMemo);
end;

destructor TUpdateThread.Destroy;
begin
  if FTermEvent <> 0 then
    CloseHandle(FTermEvent);

  inherited;
end;

procedure TUpdateThread.DoFinalize;
begin
  if Assigned(FOnDestroy) then
    FOnDestroy(Self);
end;

procedure TUpdateThread.NotifyUpdate;
begin
  FMainForm.UpdateAvailable(FNewVersion);
end;

procedure TUpdateThread.NotifyUpdateComplete;
begin
  FMainForm.UpdateComplete;
end;

function TUpdateThread.SoftWin32Check(Source: string; RetVal: BOOL): BOOL;
begin
  Result := inherited;
  if not Result then
    WriteLine(MainForm.Langs[eUpdateFailed]);
end;

function TUpdateThread.CompareVersion(LastRelease: string): Boolean;
var
  VerData: TArray<string>;
  Suffix: string;
  Major, Minor, PatchLevel: Word;
begin
  { ��������� �������� ������, ������� ����� �������� � �������:
    v<major>.<minor>.<patch_level>[-<suffix>] }
  FNewVersion := LastRelease.Substring(1);
  VerData := FNewVersion.Split(['.', '-']);

  if (Length(VerData) < 1) or not Word.TryParse(VerData[0], Major) then
    Major := 0;

  if (Length(VerData) < 2) or not Word.TryParse(VerData[1], Minor) then
    Minor := 0;

  if (Length(VerData) < 3) or not Word.TryParse(VerData[2], PatchLevel) then
    PatchLevel := 0;

  if (Length(VerData) >= 4) then
    Suffix := VerData[3]
  else
    Suffix := '';

  { ���������� }
  if Major > VersionInfo.Major then
    Exit(True)
  else if Major < VersionInfo.Major then
    Exit(False);

  if Minor > VersionInfo.Minor then
    Exit(True)
  else if Minor < VersionInfo.Minor then
    Exit(False);

  if PatchLevel > VersionInfo.PatchLevel then
    Exit(True)
  else if PatchLevel < VersionInfo.PatchLevel then
    Exit(False);

  if Suffix.IsEmpty and not VersionInfo.Suffix.IsEmpty then
    { ���� � ����� ������ ��� ��������� (-rc{x]), �� ��� ����� ����� }
    Result := True
  else
    Result := Suffix > VersionInfo.Suffix;
end;

function TUpdateThread.CheckLatestRelease(const Response: string): Boolean;
var
  Tags, Tag, Release: TJSONValue;
  TagName: string;
  i: Integer;
begin
  Result := False;
  Tags := TJSONObject.ParseJSONValue(Response);
  if Assigned(Tags) and (Tags is TJSONArray) then
    for i := 0 to (Tags as TJSONArray).Count - 1 do
    begin
      Tag := (Tags as TJSONArray).Items[i];
      if (Tag is TJSONObject) then
      begin
        Release := (Tag as TJSONObject).GetValue('release');
        if Assigned(Release) and (Release is TJSONObject) and
          Assigned((Release as TJSONObject).GetValue('tag_name')) and
          ((Release as TJSONObject).GetValue('tag_name') is TJSONString) then
        begin
          TagName := ((Release as TJSONObject).GetValue('tag_name') as TJSONString).Value;
          if not TagName.IsEmpty then
            Exit(CompareVersion(TagName));
        end;
      end;
    end;
end;

procedure TUpdateThread.TerminatedSet;
begin
  SetEvent(FTermEvent);
  inherited;
end;

procedure TUpdateThread.Execute;
const
  DEFLATE_WINDOW_BITS = -15;
  GZIP_WINDOW_BITS = 31;
  MAX_REPEAT_COUNT = 30;
  DELAY_MIN_SEC = 30;
  DELAY_MAX_SEC = 600;
var
  ResponseStream, CompressedStream: TBytesStream;
  UngzipStream: TZDecompressionStream;
  Session, Connect, Request: HINTERNET;
  ObjectName, Response, Encoding: string;
  Attempt, HttpStatusCode, ResponseSize, EncodingSize, LastError, Flags: DWORD;
  WindowBits: Integer;

  { ���� ���� ������ ��������� ������������, ��� �������� �� ������, � �� �
    ��������, �� �� ������ ������� �������... }
  function WaitForInternet: Boolean;
  begin
    if Attempt > 0 then
    begin
      Dec(Attempt);
      { �� �� ����� ����� � ���� ������ ����� Sleep, �.�. ���������� � �����
        ������ ����� ����������� � ����� ��������� ����������, � �� � ��������
        ����� �������� ����� ������ }
      WaitForSingleObject(FTermEvent,
        1000 * (DELAY_MIN_SEC + Random(DELAY_MAX_SEC - DELAY_MIN_SEC)));
      SetLastError(ERROR_SUCCESS);
      Result := True;
    end
    else
      Result := False;
  end;
begin
  try
    Session := nil;
    Connect := nil;
    Request := nil;
    ResponseStream := nil;
    CompressedStream := nil;
    UngzipStream := nil;
    Attempt := MAX_REPEAT_COUNT;
    Response := '';

    if not VersionInfo.InfoReaded then
      { ���� �� ��������� ���������� � ������ �����, �� � ������ ������ }
      Exit;

    ObjectName := Format(PROJECT_RELEASES, [TURLEncoding.URL.Encode(PROJECT_ID)]);
    try
      ResponseStream := TBytesStream.Create;
      ResponseStream.Size := 4096; { ������ ��������� ������ }

      Session := WinHttpOpen(
        PChar('Mozilla/5.0 (compatible; WinHTTP/5.1)'),
        WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
        WINHTTP_NO_PROXY_NAME,
        WINHTTP_NO_PROXY_BYPASS,
        0
      );
      if not SoftWin32Check('TUpdateThread.Execute::WinHttpOpen', Assigned(Session)) then
        Exit;

      Connect := WinHttpConnect(Session, PChar(PROJECT_HOST), INTERNET_DEFAULT_HTTPS_PORT, 0);
      if not SoftWin32Check('TUpdateThread.Execute::WinHttpConnect', Assigned(Connect)) then
        Exit;

      Request := WinHttpOpenRequest(
        Connect,
        nil,
        PChar(ObjectName),
        nil,
        WINHTTP_NO_REFERER,
        WINHTTP_DEFAULT_ACCEPT_TYPES,
        WINHTTP_FLAG_SECURE or WINHTTP_FLAG_BYPASS_PROXY_CACHE or WINHTTP_FLAG_REFRESH
      );
      if not SoftWin32Check('TUpdateThread.Execute::WinHttpOpenRequest', Assigned(Request)) then
        Exit;

      { ��������� ����� ����� ���������� - ���������� ������ }
      if CheckWin32Version(8, 1) then
      begin
        { WinHTTP ����� �������������� ������������� ���������� }
        Flags := WINHTTP_DECOMPRESSION_FLAG_ALL;
        if not SoftWin32Check('TUpdateThread.Execute::WinHttpSetOption',
          WinHttpSetOption(Request, WINHTTP_OPTION_DECOMPRESSION, @Flags, SizeOf(Flags))) then
          Exit;
      end
      else
        if not SoftWin32Check('TUpdateThread.Execute::WinHttpSetOption',
          WinHttpAddRequestHeaders(
            Request,
            PChar('Accept-Encoding: gzip,deflate'),
            DWORD(-1),
            WINHTTP_ADDREQ_FLAG_ADD or WINHTTP_ADDREQ_FLAG_REPLACE
          )) then
          Exit;

      while not Terminated do
      begin
        SetLastError(ERROR_SUCCESS);
        WinHttpSendRequest(Request, WINHTTP_NO_ADDITIONAL_HEADERS, 0,
          WINHTTP_NO_REQUEST_DATA, 0, 0, 0);
        LastError := GetLastError;
        case LastError of
          ERROR_SUCCESS: begin
            if SoftWin32Check('TUpdateThread.Execute::WinHttpReceiveResponse',
              WinHttpReceiveResponse(Request, nil)) then
            begin
              { ��������� ��� ������ HTTP }
              HttpStatusCode := 0;
              ResponseSize := SizeOf(HttpStatusCode);
              if not SoftWin32Check('TUpdateThread.Execute::WinHttpQueryHeaders(WINHTTP_QUERY_STATUS_CODE)',
                WinHttpQueryHeaders(Request, WINHTTP_QUERY_STATUS_CODE or WINHTTP_QUERY_FLAG_NUMBER,
                WINHTTP_HEADER_NAME_BY_INDEX, @HttpStatusCode, ResponseSize, WINHTTP_NO_HEADER_INDEX)) then
                Exit;

              case HttpStatusCode of
                HTTP_STATUS_OK:
                  { �������, �� �������� ���� ����� }
                  Break;
                0, HTTP_STATUS_SERVER_ERROR, HTTP_STATUS_BAD_GATEWAY,
                HTTP_STATUS_SERVICE_UNAVAIL, HTTP_STATUS_GATEWAY_TIMEOUT:
                  { ������� �������� ���� ������, ������� ����� ����������
                    ������������� ������-������� ��� ��������� � ���������� }
                  if WaitForInternet then Continue else Exit;
                else begin
                  { ������ ������, ������ ���������� }
                  if WaitForInternet then Continue else Exit; //!!!
//!                  WriteLine(Format('HTTP Error: %u', [HttpStatusCode]));
//!                  WriteLine(MainForm.Langs[eUpdateFailed]);
//!                  Exit;
                end;
              end;
            end
            else
              { � �������� �� � �������, � ��� ����� ��������� ����� }
              Exit;
          end;
          ERROR_WINHTTP_NAME_NOT_RESOLVED, ERROR_WINHTTP_CANNOT_CONNECT,
          ERROR_WINHTTP_TIMEOUT, ERROR_WINHTTP_RESEND_REQUEST:
            { ������ �� ����� ����������� � ��������. ������. }
            if WaitForInternet then Continue else Exit;
          ERROR_WINHTTP_HEADER_COUNT_EXCEEDED, ERROR_WINHTTP_HEADER_SIZE_OVERFLOW,
          ERROR_WINHTTP_INVALID_SERVER_RESPONSE,
          ERROR_WINHTTP_RESPONSE_DRAIN_OVERFLOW: begin
            { ��� � �������� ������, �� �� ����� ������� ��������� � ���������,
              �������� ���������� ��� ������-������ ����� ������ � �.�. }
            SetLastError(LastError);
            if SoftWin32Check('TUpdateThread.Execute::WinHttpSendRequest', Attempt > 0) then
              if WaitForInternet then Continue else Exit
            else
              Exit;
          end;
          else begin
            SetLastError(LastError);
            SoftWin32Check('TUpdateThread.Execute::WinHttpSendRequest', False);
            Exit;
          end;
        end;
      end;

      { ���� �� ��������� ����, �� ����� ������� }
      while not Terminated do
      begin
        ResponseSize := 0;
        if not SoftWin32Check('TUpdateThread.Execute::WinHttpQueryDataAvailable',
          WinHttpQueryDataAvailable(Request, @ResponseSize)) then
          Exit;

        if ResponseSize = 0 then
          Break;

        if ResponseStream.Size < ResponseSize + 1 then
          ResponseStream.Size := ResponseSize + 1;
        FillChar(ResponseStream.Bytes[0], ResponseSize + 1, 0);

        if not SoftWin32Check('TUpdateThread.Execute::WinHttpReadData',
          WinHttpReadData(Request, ResponseStream.Bytes[0], ResponseSize, @ResponseSize)) then
          Exit;

        if not CheckWin32Version(8, 1) then
        begin
          { ������ ������� ������������ �����, �������� ��������� ����������� }
          SetLastError(ERROR_SUCCESS);
          WinHttpQueryHeaders(Request, WINHTTP_QUERY_CONTENT_ENCODING, nil,
            WINHTTP_NO_OUTPUT_BUFFER, EncodingSize, WINHTTP_NO_HEADER_INDEX);
          LastError := GetLastError;
          case LastError of
            ERROR_WINHTTP_HEADER_NOT_FOUND:
              ; { ��� ������ ���������, ����� ������ �������� }
            ERROR_INSUFFICIENT_BUFFER: begin
              { ����� ��������, �������� ��� ��������� }
              SetLength(Encoding, EncodingSize);
              if WinHttpQueryHeaders(Request, WINHTTP_QUERY_CONTENT_ENCODING,
                WINHTTP_HEADER_NAME_BY_INDEX, @Encoding[Low(Encoding)], EncodingSize,
                WINHTTP_NO_HEADER_INDEX) then
              begin
                SetLength(Encoding, Encoding.IndexOf(#0));
                if Encoding.Contains('gzip') then
                  WindowBits := GZIP_WINDOW_BITS
                else if Encoding.Contains('deflate') then
                  WindowBits := DEFLATE_WINDOW_BITS
                else
                  WindowBits := 0;

                if WindowBits <> 0 then
                begin
                  { ��, ����� gzip, � ��������� ������ ������ �� ������ �
                    ��������, ��� ����� �� ���� }
                  if not Assigned(CompressedStream) then
                    CompressedStream := TBytesStream.Create
                  else
                    CompressedStream.Position := 0;
                  CompressedStream.Size := ResponseSize;
                  CompressedStream.CopyFrom(ResponseStream, ResponseSize);
                  CompressedStream.Position := 0;

                  Assert(not Assigned(UngzipStream));
                  UngzipStream := TZDecompressionStream.Create(CompressedStream,
                    WindowBits, False);
                  try
                    ResponseSize := UngzipStream.Size;
                    ResponseStream.Position := 0;
                    ResponseStream.CopyFrom(UngzipStream, ResponseSize);
                  finally
                    ResponseStream.Position := 0;
                    FreeAndNil(UngzipStream);
                  end;
                end;
              end
              else begin
                LastError := GetLastError;
                if LastError <> ERROR_WINHTTP_HEADER_NOT_FOUND then
                begin
                  SetLastError(LastError);
                  SoftWin32Check('TUpdateThread.Execute::WinHttpQueryHeaders(WINHTTP_QUERY_CONTENT_ENCODING)', False);
                  Exit;
                end;
              end;
            end;
            else begin
              SetLastError(LastError);
              SoftWin32Check('TUpdateThread.Execute::WinHttpQueryHeaders(WINHTTP_QUERY_CONTENT_ENCODING, Size)', False);
              Exit;
            end;
          end;
        end;

        try
          Response := Response + TEncoding.UTF8.GetString(ResponseStream.Bytes, 0, ResponseSize);
        except
          on E: Exception do
          begin
            WriteLine('TUpdateThread.Execute::' + E.ClassName + '(' + E.Message + ')');
            WriteLine(MainForm.Langs[eUpdateFailed]);
            Exit;
          end;
        end;
      end;

      if CheckLatestRelease(Response) then
        Synchronize(NotifyUpdate);
    finally
      if Assigned(Request) then
        WinHttpCloseHandle(Request);
      if Assigned(Connect) then
        WinHttpCloseHandle(Connect);
      if Assigned(Session) then
        WinHttpCloseHandle(Session);
      if Assigned(UngzipStream) then
        FreeAndNil(UngzipStream);
      if Assigned(ResponseStream) then
        FreeAndNil(ResponseStream);
      if Assigned(CompressedStream) then
        FreeAndNil(CompressedStream);
      Synchronize(NotifyUpdateComplete);
    end;
  finally
    Synchronize(DoFinalize);
  end;
end;

initialization
  VersionInfo := TVersionInfo.Create(Application.ExeName);

end.
