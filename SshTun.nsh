;   SshTun, is a small utility that allows you to turn almost any ssh server
;   into a proxy server.
;   Copyright (C) 2018  Dmitriy Pomerantsev
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <https://www.gnu.org/licenses/>.

#!define PRODUCT_VERSION_MAJOR "1"
#!define PRODUCT_VERSION_MINOR "0"
#!define PRODUCT_VERSION_PATCHLEVEL "0"
#!define PRODUCT_VERSION_SUFFIX "-rc1"
!define PRODUCT_VERSION_MAJOR "1"
!define PRODUCT_VERSION_MINOR "0"
!define PRODUCT_VERSION_PATCHLEVEL "0"
!define PRODUCT_VERSION_SUFFIX ""