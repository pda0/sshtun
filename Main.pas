(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.ExtCtrls, System.ImageList, Vcl.ImgList, Vcl.StdCtrls,
  Vcl.Menus, System.Actions, Vcl.ActnList, Langs, Plink;

type
  TMainForm = class(TForm)
    TrayIcon: TTrayIcon;
    ApplicationEvents: TApplicationEvents;
    IconList: TImageList;
    ConMemo: TMemo;
    TrayMenu: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    ActionList: TActionList;
    ConsoleAction: TAction;
    ConnectAction: TAction;
    DisconnectAction: TAction;
    SettingsAction: TAction;
    ExitAction: TAction;
    AboutAction: TAction;
    About1: TMenuItem;
    procedure ApplicationMinimize(Sender: TObject);
    procedure ExitActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ConsoleActionExecute(Sender: TObject);
    procedure ConsoleActionUpdate(Sender: TObject);
    procedure ConnectActionUpdate(Sender: TObject);
    procedure ConnectActionExecute(Sender: TObject);
    procedure DisconnectActionUpdate(Sender: TObject);
    procedure DisconnectActionExecute(Sender: TObject);
    procedure SettingsActionUpdate(Sender: TObject);
    procedure SettingsActionExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TrayIconDblClick(Sender: TObject);
    procedure ConMemoChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AboutActionUpdate(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
  private
    { Private declarations }
    FLangs: TLangs;
    FPlinkCtl: TPlinkCtl;
    FUpdateThread: TThread;
    procedure UpdateCaptions(CaptionText: string = '');
    procedure SshNotify(Sender: TObject; SshStatus: TSshStatus);
    procedure SuccessNotify(Caption, Text: string; OnClick: TNotifyEvent = nil);
    procedure FailNotify(Text: string);
    procedure OpenUpdate(Sender: TObject);
    procedure UpdateThreadComplete(Sender: TObject);
  public
    { Public declarations }
    procedure Init;
    procedure UpdateAvailable(NewVersion: string);
    procedure UpdateComplete;
    property Langs: TLangs read FLangs;
  end;

var
  MainForm: TMainForm;

resourcestring
  sUpdateMessage = 'New version (%s) available';

implementation

uses
  System.DateUtils, Settings, About, UpdateChecker;

{$R *.dfm}

resourcestring
  sConnected = 'Connected';
  sDisconnected = 'Disconnected';
  sError = 'Error';
  sUpdate = 'Update Available';
  sConnectedText = 'Connected to server: %s.';
  sDisconnectedText = 'Disconnected from server: %s.';
  eConnectFail = 'Open the console for supplemental information.';

type
  TSwitchToThisWindow = procedure(AWindow: HWND; ARestore: BOOL); stdcall;

var
  SwitchToThisWindow: TSwitchToThisWindow = nil;

procedure TMainForm.SuccessNotify(Caption, Text: string; OnClick: TNotifyEvent);
begin
  TrayIcon.OnBalloonClick := OnClick;
  TrayIcon.BalloonFlags := bfInfo;
  TrayIcon.BalloonTitle := Caption;
  TrayIcon.BalloonHint := Text;
  TrayIcon.ShowBalloonHint;
end;

procedure TMainForm.FailNotify(Text: string);
begin
  TrayIcon.BalloonFlags := bfError;
  TrayIcon.BalloonTitle := Langs[sError];
  TrayIcon.BalloonHint := Text;
  TrayIcon.ShowBalloonHint;
end;

procedure TMainForm.OpenUpdate(Sender: TObject);
begin
  AboutForm.UpdateLabelClick(AboutForm.UpdateLabel);
end;

procedure TMainForm.UpdateThreadComplete(Sender: TObject);
begin
  FUpdateThread := nil;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  SwitchToThisWindow := GetProcAddress(GetModuleHandle(user32),
    PChar('SwitchToThisWindow'));

  Application.ShowMainForm := False;
  ApplicationMinimize(Self);

  FLangs := TLangs.Create(TLangs.LANG_ENU);
  FLangs.RegisterForm(Self, nil, nil);
  FPlinkCtl := nil;
  FUpdateThread := nil;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  if Assigned(FUpdateThread) then
  begin
    FUpdateThread.Terminate;
    { �� �� ����� ����� ����� WaitFor, ������ ��� ����� ��� ���� ������ }
    while Assigned(FUpdateThread) do
    begin
      Sleep(100);
      Application.ProcessMessages;
    end;
  end;
  FreeAndNil(FLangs);
end;

procedure TMainForm.Init;
var
  LaunchDate: TDateTime;
begin
  SettingsForm.LoadConfig;

  { �������� ���������� }
  LaunchDate := TTimeZone.Local.ToUniversalTime(Now);
  if SettingsForm.CheckForUpdates and
    (DaysBetween(LaunchDate, SettingsForm.UpdateDate) >= 1) then
    FUpdateThread := TUpdateThread.Create(Self, ConMemo, UpdateThreadComplete);

  { ���������� }
  if SettingsForm.AutoConnect and ConnectAction.Enabled then
    ConnectAction.Execute;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caNone;
  Hide;
end;

procedure TMainForm.UpdateCaptions(CaptionText: string);
begin
  if CaptionText.IsEmpty then
    Caption := 'SshTun'
  else
    Caption := 'SshTun: ' + CaptionText;

  TrayIcon.Hint := Caption;
end;

procedure TMainForm.SshNotify(Sender: TObject; SshStatus: TSshStatus);
var
  ServerStr: string;
begin
  ServerStr := SettingsForm.Host;
  if SettingsForm.Port <> SettingsForm.DEFAULT_SSH_PORT then
    ServerStr := ServerStr + ':' + SettingsForm.Port.ToString;

  case SshStatus of
    TSshStatus.Connected: begin
      SuccessNotify(Langs[sConnected], Format(Langs[sConnectedText], [ServerStr]));
      TrayIcon.IconIndex := 1;
      UpdateCaptions(SettingsForm.Login + '@' + ServerStr);
    end;
    TSshStatus.Disconnected: begin
      SuccessNotify(Langs[sDisconnected], Format(Langs[sDisconnectedText], [ServerStr]));
      DisconnectAction.Execute;
    end;
    TSshStatus.Failed: begin
      DisconnectAction.Execute;
      FailNotify(Langs[eConnectFail]);
    end;
  end;
end;

procedure TMainForm.UpdateAvailable(NewVersion: string);
begin
  AboutForm.UpdateAvailable(NewVersion);
  SuccessNotify(Langs[sUpdate], Format(Langs[sUpdateMessage], [NewVersion]),
    OpenUpdate);
end;

procedure TMainForm.UpdateComplete;
begin
  SettingsForm.SetUpdateDate;
end;

procedure TMainForm.TrayIconDblClick(Sender: TObject);
begin
  ConnectAction.Update;

  if ConnectAction.Enabled then
    ConnectAction.Execute
  else
    DisconnectAction.Execute;
end;

procedure TMainForm.ApplicationMinimize(Sender: TObject);
begin
  Hide;
end;

procedure TMainForm.ConsoleActionUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not Visible;
end;

procedure TMainForm.ConsoleActionExecute(Sender: TObject);
begin
  Show;
  if Assigned(SwitchToThisWindow) then
    SwitchToThisWindow(Handle, True)
  else begin
    ShowWindow(Handle, SW_RESTORE);
    SetForegroundWindow(Handle);
  end;
end;

procedure TMainForm.ConnectActionUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not (Assigned(FPlinkCtl) or SettingsForm.Visible) and
    SettingsForm.CheckConfig(ConMemo);
end;

procedure TMainForm.ConMemoChange(Sender: TObject);
begin
  while ConMemo.Lines.Count > 300 do
    ConMemo.Lines.Delete(0);

  ConMemo.SelLength := 0;
  ConMemo.SelStart := Length(ConMemo.Text);
end;

procedure TMainForm.ConnectActionExecute(Sender: TObject);
begin
  FPlinkCtl := TPlinkCtl.Create(SettingsForm, ConMemo, SshNotify);
end;

procedure TMainForm.DisconnectActionUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := Assigned(FPlinkCtl);
end;

procedure TMainForm.DisconnectActionExecute(Sender: TObject);
begin
  TrayIcon.IconIndex := 0;
  UpdateCaptions;
  if Assigned(FPlinkCtl) then
  begin
    FPlinkCtl.Terminate;
    FPlinkCtl := nil;
  end;
end;

procedure TMainForm.SettingsActionUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not (Assigned(FPlinkCtl) or SettingsForm.Visible);
end;

procedure TMainForm.SettingsActionExecute(Sender: TObject);
begin
  if SettingsForm.ShowModal = mrOk then
    SettingsForm.SaveConfig;
end;

procedure TMainForm.AboutActionUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not AboutForm.Visible;
end;

procedure TMainForm.AboutActionExecute(Sender: TObject);
begin
  AboutForm.ShowModal;
end;

procedure TMainForm.ExitActionExecute(Sender: TObject);
var
  PlinkThread: TThread;
begin
  PlinkThread := FPlinkCtl;
  if Assigned(PlinkThread) then
  begin
    PlinkThread.FreeOnTerminate := False;
    DisconnectAction.Execute;
    PlinkThread.WaitFor;
    FreeAndNil(PlinkThread);
  end;

  Application.Terminate;
end;

end.
