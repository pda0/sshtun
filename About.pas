(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
unit About;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TAboutForm = class(TForm)
    SshTunLabel: TLabel;
    VersionLabel: TLabel;
    OkButton: TButton;
    NoticeLabel: TLabel;
    License: TRichEdit;
    UpdateLabel: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure UpdateLabelMouseEnter(Sender: TObject);
    procedure UpdateLabelMouseLeave(Sender: TObject);
    procedure UpdateLabelClick(Sender: TObject);
  private
    { Private declarations }
    FNewVersion: string;
    procedure UpdateLink;
    procedure AfterUpdate(Sender: TObject);
  public
    { Public declarations }
    procedure UpdateAvailable(NewVersion: string);
  end;

var
  AboutForm: TAboutForm;

implementation

uses
  Winapi.ShellAPI, System.UITypes, Main, UpdateChecker;

{$R *.dfm}

{$WARN USE_BEFORE_DEF OFF}
procedure TAboutForm.FormCreate(Sender: TObject);
var
  PatchSuffix: string;
begin
  if VersionInfo.InfoReaded then
  begin
    if VersionInfo.Suffix.IsEmpty then
      PatchSuffix := VersionInfo.PatchLevel.ToString
    else
      PatchSuffix := VersionInfo.PatchLevel.ToString + '-' + VersionInfo.Suffix;

    VersionLabel.Caption := Format('%u.%u.%s (build: %u).',
      [VersionInfo.Major, VersionInfo.Minor, PatchSuffix, VersionInfo.Build]);
  end
  else
    VersionLabel.Caption := '';

  MainForm.Langs.RegisterForm(Self, nil, AfterUpdate);
end;
{$WARN USE_BEFORE_DEF DEFAULT}

procedure TAboutForm.AfterUpdate(Sender: TObject);
begin
  if UpdateLabel.Visible then
    UpdateLink;
end;

procedure TAboutForm.UpdateAvailable(NewVersion: string);
begin
  FNewVersion := NewVersion;
  UpdateLink;
  UpdateLabel.Visible := True;
end;

procedure TAboutForm.UpdateLink;
begin
  UpdateLabel.Caption := Format(MainForm.Langs[sUpdateMessage], [FNewVersion]);
end;

procedure TAboutForm.UpdateLabelMouseEnter(Sender: TObject);
begin
  UpdateLabel.Font.Style := UpdateLabel.Font.Style + [TFontStyle.fsUnderline];
end;

procedure TAboutForm.UpdateLabelMouseLeave(Sender: TObject);
begin
  UpdateLabel.Font.Style := UpdateLabel.Font.Style - [TFontStyle.fsUnderline];
end;

procedure TAboutForm.UpdateLabelClick(Sender: TObject);
begin
  ShellExecute(0, 'Open', PChar(Format(URL_RELEASES, [PROJECT_HOST, PROJECT_ID])),
    nil, nil, SW_SHOWNORMAL);
end;



end.
