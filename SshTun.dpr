(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
program SshTun;

uses
  Vcl.Forms,
  Main in 'Main.pas' {MainForm},
  Settings in 'Settings.pas' {SettingsForm},
  ConThread in 'ConThread.pas',
  Plink in 'Plink.pas',
  Langs in 'Langs.pas',
  About in 'About.pas' {AboutForm},
  UpdateChecker in 'UpdateChecker.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TAboutForm, AboutForm);
  Application.CreateForm(TSettingsForm, SettingsForm);
  Application.Run;
end.
