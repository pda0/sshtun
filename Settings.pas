﻿(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
unit Settings;

{$WARN SYMBOL_PLATFORM OFF}
{$WARN UNIT_PLATFORM OFF}

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, System.ImageList, Vcl.ImgList,
  System.Win.Registry, Vcl.ComCtrls, System.Generics.Collections;

type
  TAuthType = (Pageant, Key, Password);

  TSettingsForm = class(TForm)
    KeyFileDialog: TFileOpenDialog;
    OkButton: TButton;
    CancelButton: TButton;
    ImageList: TImageList;
    SettingsPages: TPageControl;
    ConSheet: TTabSheet;
    CommonSheet: TTabSheet;
    PortLabel: TLabel;
    PortEdit: TEdit;
    LoginLabel: TLabel;
    UserEdit: TEdit;
    AuthLabel: TLabel;
    AuthTypeComboBox: TComboBox;
    AuthTextLabel: TLabel;
    AuthEdit: TButtonedEdit;
    LocalPortLabel: TLabel;
    LocalPortEdit: TEdit;
    InterfaceBox: TGroupBox;
    LangLabel: TLabel;
    LangComboBox: TComboBox;
    LaunchBox: TGroupBox;
    AutoConnectCheckBox: TCheckBox;
    AutoRunCheckBox: TCheckBox;
    HostEdit: TEdit;
    HostLabel: TLabel;
    ExternalBox: TGroupBox;
    PuTTYLabel: TLabel;
    PuTTYEdit: TButtonedEdit;
    UpdateBox: TGroupBox;
    UpdatesCheckBox: TCheckBox;
    procedure AuthEditRightButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AuthTypeComboBoxChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OkButtonClick(Sender: TObject);
    procedure PuTTYEditRightButtonClick(Sender: TObject);
  private const
    AUTH_PAGEANT = 0;
    AUTH_KEY = 1;
    AUTH_PASSWORD = 2;
    ICO_KEY = 0;
    ICO_PASS_HIDDEN = 1;
    ICO_PASS_SHOWED = 2;
    PASSWORD_CHAR: Char = Char('●');
  private type
    TLocInfo = record
      DisplayName: string;
      Locale: LCID;
    end;
    TConnectionInfo = record
      Host, Login, AuthText: string;
      Port, LocalPort: Integer;
      AuthType: TAuthType;
    end;
  private
    { Private declarations }
    FDefaultConnection: TConnectionInfo;
    FLocales: TDictionary<string, TLocInfo>;
    FUpdateDate: TDateTime;
    FPuTTYPath: string;
    FLocaleNameType: LCTYPE;
    FAutoConnect, FAutoRun, FCheckUpdates, FChecked: Boolean;
    class function GetRegString(const Reg: TRegistry; Name, Default: string): string; static;
    class function GetRegInteger(const Reg: TRegistry; Name: string; Default: Integer): Integer; static;
    class function GetRegBoolean(const Reg: TRegistry; Name: string; Default: Boolean): Boolean; static;
    class function EncryptPassword(const Password: string): string; static;
    class function DecryptPassword(const CryptedPassword: string): string; static;
    function GetHost: string;
    function GetPort: Integer;
    function GetLogin: string;
    function GetAuthType: TAuthType;
    function GetAuthText: string;
    function GetLocalPort: Integer;
    function GetAutoConnect: Boolean;
    procedure SetError(const ErrMemo: TMemo; ErrText: string; var Res: Boolean);
    function SearchPuTTYPath: string;
    procedure ConfigToForm;
    procedure FormToConfig;
    procedure RefreshTranslations;
  public const
    DEFAULT_SSH_PORT = 22;
    DEFAULT_LOCAL_PORT = 1080;
  public
    { Public declarations }
    procedure LoadConfig;
    procedure SaveConfig;
    procedure SetUpdateDate;
    function CheckConfig(const ErrMemo: TMemo): Boolean;
    property PuTTYPath: string read FPuTTYPath;
    property UpdateDate: TDateTime read FUpdateDate;
    property Host: string read GetHost;
    property Port: Integer read GetPort;
    property Login: string read GetLogin;
    property AuthType: TAuthType read GetAuthType;
    property AuthText: string read GetAuthText;
    property LocalPort: Integer read GetLocalPort;
    property AutoConnect: Boolean read GetAutoConnect;
    property CheckForUpdates: Boolean read FCheckUpdates;
  end;

var
  SettingsForm: TSettingsForm;

implementation

uses
  System.Types, System.IOUtils, System.NetEncoding, System.Hash, Vcl.FileCtrl,
  System.DateUtils, Main, Plink, Langs;

resourcestring
  sSelectPuTTY = 'Select path to PuTTY folder';
  sPuTTYOK = 'OK';
  sAutoLang = 'Automatically';
  sKeyPageant = 'Path to key file (or empty):';
  sKeyPass = 'Path to key file:';
  sPassword = 'Password:';
  sPuTTYDwn = 'Download it at %s.';
  sPuTTYUrl = 'https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html';

  eSshNoHost = 'Ssh host address is not defined.';
  eSshBadPort = 'Ssh port value is out of range.';
  eSshNoUser = 'Ssh user name is not defined.';
  eSshNoKey = 'Ssh key is not defined.';
  eSshBadKey = 'Selected ssh key is not exists or can not be read.';
  eSshBadLocalPort = 'Ssh local port value is out of range.';

{$R *.dfm}

const
{$IFNDEF crypt32}
  crypt32 = 'crypt32.dll';
{$ENDIF}
{$IFNDEF STATUS_SUCCESS}
  STATUS_SUCCESS = 0;
{$ENDIF}
{$IFNDEF CRYPTPROTECTMEMORY_BLOCK_SIZE}
  CRYPTPROTECTMEMORY_BLOCK_SIZE = 16;
{$ENDIF}
{$IFNDEF CRYPTPROTECTMEMORY_SAME_LOGON}
  CRYPTPROTECTMEMORY_SAME_LOGON = $2;
{$ENDIF}
{$IFNDEF RTL_ENCRYPT_MEMORY_SIZE}
  RTL_ENCRYPT_MEMORY_SIZE = 8;
{$ENDIF}
{$IFNDEF RTL_ENCRYPT_OPTION_SAME_LOGON}
  RTL_ENCRYPT_OPTION_SAME_LOGON = $2;
{$ENDIF}
  PWD_OFFSET_LEN = 0;
  PWD_OFFSET_HASH = SizeOf(DWORD);
  PWD_OFFSET_DATA = PWD_OFFSET_HASH + SizeOf(Integer);

{$IFNDEF CryptProtectMemory}
type
  TCryptProtectMemory = function(pData: Pointer; cbData: DWORD; dwFlags: DWORD):
    Boolean; stdcall;
var
  CryptProtectMemory: TCryptProtectMemory = nil;
{$ENDIF}

{$IFNDEF CryptUnprotectMemory}
type
  TCryptUnprotectMemory = function(pData: Pointer; cbData: DWORD; dwFlags:
    DWORD): Boolean; stdcall;
var
  CryptUnprotectMemory: TCryptUnprotectMemory = nil;
{$ENDIF}

type
  {$IFNDEF NTSTATUS}
  NTSTATUS = {$IFDEF WIN32}DWORD{$ELSE}UInt64{$ENDIF};
  {$ENDIF}
  TRtlEncryptMemory = function(Memory: Pointer; MemoryLength: ULONG;
    OptionFlags: ULONG): NTSTATUS; stdcall;
  TRtlDecryptMemory = function(Memory: Pointer; MemoryLength: ULONG;
    OptionFlags: ULONG): NTSTATUS; stdcall;

  TAsInheritedReader = class(TReader)
  public
    procedure ReadPrefix(var Flags: TFilerFlags; var AChildPos: Integer); override;
  end;

var
  CryptoLib: THandle = 0;
  RtlEncryptMemory: TRtlEncryptMemory = nil;
  RtlDecryptMemory: TRtlDecryptMemory = nil;

{ TAsInheritedReader }

procedure TAsInheritedReader.ReadPrefix(var Flags: TFilerFlags;
  var AChildPos: Integer);
begin
  inherited ReadPrefix(Flags, AChildPos);
  Include(Flags, ffInherited);
end;

{ TSettingsForm }

class function TSettingsForm.GetRegString(const Reg: TRegistry; Name, Default: string): string;
begin
  try
    Result := Reg.ReadString(Name);
  except
    Result := Default;
  end;
end;

class function TSettingsForm.GetRegInteger(const Reg: TRegistry; Name: string; Default: Integer): Integer;
begin
  try
    Result := Reg.ReadInteger(Name);
  except
    Result := Default;
  end;
end;

class function TSettingsForm.GetRegBoolean(const Reg: TRegistry; Name: string; Default: Boolean): Boolean;
begin
  try
    Result := Reg.ReadBool(Name);
  except
    Result := Default;
  end;
end;

class function TSettingsForm.EncryptPassword(const Password: string): string;
var
  EncBuffer: array of Byte;
  DataSize, BuffSize, BuffBlockSize: DWORD;
  PwdHash: Integer;
begin
  DataSize := Password.Length * SizeOf(Char);
  BuffSize := DataSize + SizeOf(DWORD) + SizeOf(Integer);

  if Assigned(CryptProtectMemory) and Assigned(CryptUnprotectMemory) then
    BuffBlockSize := CRYPTPROTECTMEMORY_BLOCK_SIZE
  else
    BuffBlockSize := RTL_ENCRYPT_MEMORY_SIZE;

  if BuffSize mod BuffBlockSize <> 0 then
    BuffSize := BuffSize + (BuffBlockSize - (BuffSize mod BuffBlockSize));

  SetLength(EncBuffer, BuffSize);
  FillChar(EncBuffer[0], Length(EncBuffer), 0);
  Move(DataSize, EncBuffer[PWD_OFFSET_LEN], SizeOf(DataSize));
  PwdHash := THashBobJenkins.GetHashValue(Password);
  Move(PwdHash, EncBuffer[PWD_OFFSET_HASH], SizeOf(PwdHash));
  Move(Password[Low(Password)], EncBuffer[PWD_OFFSET_DATA], DataSize);


  if Assigned(CryptProtectMemory) and Assigned(CryptUnprotectMemory) then
    Win32Check(CryptProtectMemory(@EncBuffer[0], BuffSize, CRYPTPROTECTMEMORY_SAME_LOGON))
  else
    Win32Check(RtlEncryptMemory(@EncBuffer[0], BuffSize, RTL_ENCRYPT_OPTION_SAME_LOGON) = STATUS_SUCCESS);

  Result := TNetEncoding.Base64.EncodeBytesToString(EncBuffer);
  FillChar(EncBuffer[0], Length(EncBuffer), 0);
end;

class function TSettingsForm.DecryptPassword(const CryptedPassword: string): string;
var
  EncBuffer: TBytes;
  DataSize, BuffSize: DWORD;
  PwdHash: Integer;
begin
  if not CryptedPassword.IsEmpty then
  begin
    EncBuffer := TNetEncoding.Base64.DecodeStringToBytes(CryptedPassword);
    BuffSize := Length(EncBuffer);
    if Assigned(CryptProtectMemory) and Assigned(CryptUnprotectMemory) then
    begin
      if not CryptUnprotectMemory(@EncBuffer[0], BuffSize,
        CRYPTPROTECTMEMORY_SAME_LOGON) then
        Exit(#0);
    end
    else begin
      if not RtlDecryptMemory(@EncBuffer[0], BuffSize,
        RTL_ENCRYPT_OPTION_SAME_LOGON) = STATUS_SUCCESS then
        Exit(#0);
    end;

    Move(EncBuffer[PWD_OFFSET_LEN], DataSize, SizeOf(DataSize));
    if (DataSize + SizeOf(DWORD) + SizeOf(Integer) > BuffSize) or
      (DataSize mod 2 <> 0) then
      Exit(#0);

    Move(EncBuffer[PWD_OFFSET_HASH], PwdHash, SizeOf(PwdHash));
    SetLength(Result, DataSize div 2);
    Move(EncBuffer[PWD_OFFSET_DATA], Result[Low(Result)], DataSize);
    FillChar(EncBuffer[0], Length(EncBuffer), 0);

    if THashBobJenkins.GetHashValue(Result) <> PwdHash then
      Exit(#0);
  end
  else
    Exit(#0);
end;

procedure TSettingsForm.RefreshTranslations;
var
  LocaleInfo: TLocInfo;
  ExeName, ExePath, ResFile: string;
begin
  LangComboBox.Clear;

  ExeName := Application.ExeName;
  {$IFDEF DEBUG}
  ExePath := ExtractFilePath(ExeName) + '..\..';
  {$ELSE}
  ExePath := ExtractFilePath(ExeName);
  {$ENDIF}
  LangComboBox.Items.BeginUpdate;
  try
    { Файла с этой локализацией не существует, потому что она вшита в exe-файл }
    if FLocales.TryGetValue('enu', LocaleInfo) then
      LangComboBox.Items.AddObject(LocaleInfo.DisplayName,
        TObject(LocaleInfo.Locale));

    { Проверяем какие файлы с локализацией доступны }
    for ResFile in TDirectory.GetFiles(ExePath,
      ChangeFileExt(ExtractFileName(ExeName), '.*')) do
      if FLocales.TryGetValue(ExtractFileExt(ResFile).Substring(1),
        LocaleInfo) then
        LangComboBox.Items.AddObject(LocaleInfo.DisplayName,
          TObject(LocaleInfo.Locale));

    LangComboBox.Sorted := True;
    LangComboBox.Sorted := False;

    LangComboBox.Items.InsertObject(0, MainForm.Langs[sAutoLang], nil);

    LangComboBox.ItemIndex := LangComboBox.Items.IndexOfObject(TObject(MainForm.Langs.Lang));
  finally
    LangComboBox.Items.EndUpdate;
  end;
end;

function TSettingsForm.GetHost: string;
begin
  Result := FDefaultConnection.Host;
end;

function TSettingsForm.GetPort: Integer;
begin
  Result := FDefaultConnection.Port;
end;

function TSettingsForm.GetLogin: string;
begin
  Result := FDefaultConnection.Login;
end;

function TSettingsForm.GetAuthType: TAuthType;
begin
  Result := FDefaultConnection.AuthType;
end;

function TSettingsForm.GetAuthText: string;
begin
  Result := FDefaultConnection.AuthText;
end;

function TSettingsForm.GetLocalPort: Integer;
begin
  Result := FDefaultConnection.LocalPort;
end;

function TSettingsForm.GetAutoConnect: Boolean;
begin
  Result := FAutoConnect;
end;

procedure TSettingsForm.FormCreate(Sender: TObject);
  function LocalesCallback(Name: PChar): Integer; stdcall;
  var
    LocaleInfo: TLocInfo;
  begin
    LocaleInfo.Locale := StrToInt('$' + string(Name));
    LocaleInfo.DisplayName := MainForm.Langs.LocaleInfo(LocaleInfo.Locale,
      SettingsForm.FLocaleNameType);

    SettingsForm.FLocales.AddOrSetValue(
      MainForm.Langs.LocaleInfo(LocaleInfo.Locale, LOCALE_SABBREVLANGNAME).ToLowerInvariant,
      LocaleInfo
    );

    Result := 1;
  end;
begin
  FChecked := False;
  MainForm.Langs.RegisterForm(Self, nil, nil);

  FLocales := TDictionary<string, TLocInfo>.Create;
  if CheckWin32Version(6, 1) then
    { Windows 7+ }
    FLocaleNameType := LOCALE_SLOCALIZEDDISPLAYNAME
  else if CheckWin32Version(6, 0) then
    { Windows Vista }
    FLocaleNameType := LOCALE_SLOCALIZEDLANGUAGENAME
  else
    { Windows XP }
    FLocaleNameType := LOCALE_SLANGUAGE;
  EnumSystemLocales(@LocalesCallback, LCID_SUPPORTED);
  RefreshTranslations;

  MainForm.Init;
end;

procedure TSettingsForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FLocales);
end;

procedure TSettingsForm.FormShow(Sender: TObject);
begin
  RefreshTranslations;

  { Копируем данные конфигурации в форму, смена языка сбрасывает форму в
    значение по умолчанию }
  ConfigToForm;
end;

function TSettingsForm.SearchPuTTYPath: string;
var
  Reg: TRegistry;
  Values: TStrings;
  Path: string;

  function CheckForPuTTY(var Path: string): Boolean;
  begin
    if Path.IsEmpty then
      Exit(False);

    Path := IncludeTrailingPathDelimiter(Path);
    Result := FileExists(Path + 'plink.exe');
  end;

  function GetPathFromCommand(RegPath: string): string;
  begin
    Reg.CloseKey;
    if Reg.OpenKeyReadOnly(RegPath) then
      Result := TPath.GetDirectoryName(GetRegString(Reg, '', '').
        DeQuotedString('"'))
    else
      Result := '';
  end;

  function ExpandEnvStr(const EnvString: string): string;
  const
    MAXSIZE = 32768;
  begin
    SetLength(Result, MAXSIZE);
    SetLength(Result, ExpandEnvironmentStrings(
      PChar(EnvString), @Result[Low(Result)], Result.Length) - 1
    );
  end;
begin
  { Пытаемся найти установленный PuTTY }
  { 1. Проверяем каталоги PATH, поскольку PuTTY устанавливает себя туда,
    ради pscp }
  for Path in string(GetEnvironmentVariable('PATH')).Split([TPath.PathSeparator],
    TStringSplitOptions.ExcludeEmpty) do
  begin
    Result := Path; { Path здесь const, и не может быть передан как var-параметр }
    if CheckForPuTTY(Result) then Exit;
  end;

  { 2. Проверяем пути установки по умолчанию }
  Result := IncludeTrailingPathDelimiter(GetEnvironmentVariable('PROGRAMFILES')) + 'PuTTY';
  if CheckForPuTTY(Result) then Exit;
  {$IFDEF CPU32}
  Result := IncludeTrailingPathDelimiter(GetEnvironmentVariable('ProgramW6432')) + 'PuTTY';
  {$ELSE}
  Result := IncludeTrailingPathDelimiter(GetEnvironmentVariable('ProgramFiles(x86)')) + 'PuTTY';
  {$ENDIF}
  if CheckForPuTTY(Result) then Exit;

  Values := nil;
  Reg := TRegistry.Create(KEY_READ);
  try
    { 3. Пробуем получить путь к PuTTY через файловую ассоциацию ppk }
    Reg.RootKey := HKEY_CLASSES_ROOT;
    if Reg.OpenKeyReadOnly('.ppk') then
    begin
      { Проверяем mime-тип, чтобы убедиться, что ассоциация правильная }
      if GetRegString(Reg, 'Content Type', '') = 'application/x-putty-private-key' then
      begin
        { Читаем значение по умолчанию, которое укажет нам где искать информацию }
        Path := GetRegString(Reg, '', '');
        if not Path.IsEmpty then
        begin
          Result := GetPathFromCommand(Path + '\shell\edit\command');
          if CheckForPuTTY(Result) then Exit;

          Result := GetPathFromCommand(Path + '\shell\open\command');
          if CheckForPuTTY(Result) then Exit;
        end;
      end;

      Reg.CloseKey;
    end;

    { 4. Может быть у пользователя установлен WinSCP, он может хранить в реестре
      путь к PuTTY }
    Reg.RootKey := HKEY_CURRENT_USER;
    if Reg.OpenKeyReadOnly('Software\Martin Prikryl\WinSCP 2\Configuration\Interface') then
    begin
      Path := GetRegString(Reg, 'PuttyPath', '');
      if not Path.IsEmpty then
      begin
        Result := TPath.GetDirectoryName(ExpandEnvStr(TURLEncoding.URL.Decode(Path)));
        if CheckForPuTTY(Result) then Exit;
      end;

      Reg.CloseKey;
    end;

    { 5. Если PuTTY был установлен, то в каталогах установщика может быть путь
      к нему }
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKeyReadOnly('SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\Folders') then
    begin
      Values := TStringList.Create;
      Reg.GetValueNames(Values);
      for Path in Values do
        if Path.Contains('PuTTY') then
        begin
          Result := Path; { Path здесь const, и не может быть передан как var-параметр }
          if CheckForPuTTY(Result) then Exit;
        end;

      Reg.CloseKey;
    end;
  finally
    FreeAndNil(Values);
    FreeAndNil(Reg);
  end;

  Result := '';
end;

procedure TSettingsForm.ConfigToForm;
begin
  HostEdit.Text := FDefaultConnection.Host;
  PortEdit.Text := FDefaultConnection.Port.ToString;
  UserEdit.Text := FDefaultConnection.Login;
  case FDefaultConnection.AuthType of
    TAuthType.Pageant:
      AuthTypeComboBox.ItemIndex := AUTH_PAGEANT;
    TAuthType.Key:
      AuthTypeComboBox.ItemIndex := AUTH_KEY;
    TAuthType.Password:
      AuthTypeComboBox.ItemIndex := AUTH_PASSWORD;
    else
      AuthTypeComboBox.ItemIndex := AUTH_PASSWORD;
  end;
  AuthTypeComboBoxChange(AuthTypeComboBox);
  AuthEdit.Text := FDefaultConnection.AuthText;
  LocalPortEdit.Text := FDefaultConnection.LocalPort.ToString;

  PuTTYEdit.Text := ExcludeTrailingPathDelimiter(FPuTTYPath);
  AutoConnectCheckBox.Checked := FAutoConnect;
  AutoRunCheckBox.Checked := FAutoRun;
  UpdatesCheckBox.Checked := FCheckUpdates;
end;

procedure TSettingsForm.FormToConfig;
begin
  FAutoConnect := AutoConnectCheckBox.Checked;
  FCheckUpdates := UpdatesCheckBox.Checked;

  FDefaultConnection.Host := HostEdit.Text;
  if not (TryStrToInt(PortEdit.Text, FDefaultConnection.Port) or
    (FDefaultConnection.Port <= 0)) then
    FDefaultConnection.Port := DEFAULT_SSH_PORT;
  FDefaultConnection.Login := UserEdit.Text;
  FDefaultConnection.AuthText := AuthEdit.Text;
  if not (TryStrToInt(LocalPortEdit.Text, FDefaultConnection.LocalPort) or
    (FDefaultConnection.LocalPort <= 0)) then
    FDefaultConnection.LocalPort := DEFAULT_LOCAL_PORT;
end;

procedure TSettingsForm.LoadConfig;
var
  Conf: TRegistry;
  Passwd: string;
  Locale: LCID;
  LocaleIndex: Integer;
begin
  FChecked := False;
  Conf := TRegistry.Create;
  try
    Conf.RootKey := HKEY_CURRENT_USER;
    if Conf.OpenKey('Software\SshTun', True) then
    begin
      Locale := LCID(GetRegInteger(Conf, 'Locale', 0));
      LocaleIndex := LangComboBox.Items.IndexOfObject(TObject(Locale));
      if LocaleIndex < 0 then
      begin
        Locale := 0;
        LocaleIndex := 0;
      end;

      if Conf.ValueExists('UpdateDate') then
      begin
        if not TryISO8601ToDate(GetRegString(Conf, 'UpdateDate', ''), FUpdateDate, True) then
          FUpdateDate := 0;
      end
      else
        FUpdateDate := 0;

      FAutoConnect := GetRegBoolean(Conf, 'AutoConnect', False);
      FCheckUpdates := GetRegBoolean(Conf, 'CheckForUpdates', True);
      if not Conf.ValueExists('PuTTYPath') then
      begin
        FPuTTYPath := SearchPuTTYPath;
        Conf.WriteString('PuTTYPath', FPuTTYPath);
      end
      else begin
        FPuTTYPath := GetRegString(Conf, 'PuTTYPath', '');
        if not FPuTTYPath.IsEmpty then
          FPuTTYPath := IncludeTrailingPathDelimiter(FPuTTYPath);
      end;
    end
    else begin
      Locale := 0;
      LocaleIndex := 0;

      FAutoConnect := False;
      FCheckUpdates := True;
    end;
    Conf.CloseKey;

    LangComboBox.ItemIndex := LocaleIndex;
    MainForm.Langs.Lang := Locale;

    if Conf.OpenKey('Software\SshTun\Connections\Default', False) then
    begin
      FDefaultConnection.Host := GetRegString(Conf, 'Host', '');
      FDefaultConnection.Port := GetRegInteger(Conf, 'Port', DEFAULT_SSH_PORT);
      FDefaultConnection.Login := GetRegString(Conf, 'Login', '');
      FDefaultConnection.AuthType := TAuthType(GetRegInteger(Conf, 'AuthType',
        Integer(TAuthType.Pageant)));
      case FDefaultConnection.AuthType of
        TAuthType.Pageant, TAuthType.Key:
          FDefaultConnection.AuthText := GetRegString(Conf, 'AuthData', '');
        TAuthType.Password: begin
          Passwd := DecryptPassword(GetRegString(Conf, 'AuthData', ''));
          if Passwd <> #0 then
          begin
            FDefaultConnection.AuthText := Passwd;
            FillChar(Passwd[Low(Passwd)], Passwd.Length * SizeOf(Char), 0);
          end
          else
            FDefaultConnection.AuthText := '';
        end;
        else begin
          FDefaultConnection.AuthType := TAuthType.Pageant;
          FDefaultConnection.AuthText := '';
        end;
      end;
      FDefaultConnection.LocalPort := GetRegInteger(Conf, 'LocalPort', DEFAULT_LOCAL_PORT);
    end
    else begin
      FDefaultConnection.Host := '';
      FDefaultConnection.Port := DEFAULT_SSH_PORT;
      FDefaultConnection.Login := '';
      FDefaultConnection.AuthType := TAuthType.Pageant;
      FDefaultConnection.AuthText := '';
      FDefaultConnection.LocalPort := DEFAULT_LOCAL_PORT;
    end;
    Conf.CloseKey;

    if Conf.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', False) then
      FAutoRun := Conf.ValueExists('SshTun')
    else
      FAutoRun := False;
  finally
    FreeAndNil(Conf);
  end;
end;

procedure TSettingsForm.OkButtonClick(Sender: TObject);
begin
  FormToConfig;
end;

procedure TSettingsForm.SaveConfig;
var
  Conf: TRegistry;
  Locale: LCID;
begin
  Conf := TRegistry.Create;
  try
    Conf.RootKey := HKEY_CURRENT_USER;
    Conf.OpenKey('Software\SshTun', True);

    if LangComboBox.ItemIndex >0 then
      Locale := LCID(LangComboBox.Items.Objects[LangComboBox.ItemIndex])
    else
      Locale := 0;
    if Locale <> MainForm.Langs.Lang then
    begin
      MainForm.Langs.Lang := Locale;
      ConfigToForm;
    end;
    Conf.WriteInteger('Locale', Integer(Locale));
    Conf.WriteString('PuTTYPath', ExcludeTrailingPathDelimiter(FPuTTYPath));
    Conf.WriteBool('AutoConnect', FAutoConnect);
    Conf.WriteBool('CheckForUpdates', FCheckUpdates);

    Conf.CloseKey;
    Conf.OpenKey('Software\SshTun\Connections\Default', True);

    Conf.WriteString('Host', FDefaultConnection.Host);
    Conf.WriteInteger('Port', FDefaultConnection.Port);
    Conf.WriteString('Login', FDefaultConnection.Login);
    Conf.WriteInteger('AuthType', Integer(FDefaultConnection.AuthType));
    case FDefaultConnection.AuthType of
      TAuthType.Pageant, TAuthType.Key:
        Conf.WriteString('AuthData', FDefaultConnection.AuthText);
      TAuthType.Password:
        Conf.WriteString('AuthData', EncryptPassword(FDefaultConnection.AuthText));
      else
        Conf.WriteString('AuthData', '');
    end;
    Conf.WriteInteger('LocalPort', FDefaultConnection.LocalPort);

    Conf.CloseKey;
    if Conf.OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', False) then
      if AutoRunCheckBox.Checked then
        Conf.WriteString('SshTun', Application.ExeName.QuotedString)
      else
        Conf.DeleteValue('SshTun');
  finally
    FreeAndNil(Conf);
  end;
  FChecked := False;
end;

procedure TSettingsForm.SetUpdateDate;
var
  Conf: TRegistry;
begin
  Conf := TRegistry.Create;
  try
    Conf.RootKey := HKEY_CURRENT_USER;
    Conf.OpenKey('Software\SshTun', True);

    FUpdateDate := TTimeZone.Local.ToUniversalTime(Now);
    Conf.WriteString('UpdateDate', DateToISO8601(FUpdateDate));
  finally
    FreeAndNil(Conf);
  end;
end;

procedure TSettingsForm.SetError(const ErrMemo: TMemo; ErrText: string; var Res: Boolean);
begin
  Res := False;
  if not FChecked and Assigned(ErrMemo) then
    ErrMemo.Lines.Add(ErrText);
end;

function TSettingsForm.CheckConfig(const ErrMemo: TMemo): Boolean;
var
  FileName: string;
  Dummy: Boolean;
begin
  Result := True;

  if Host.IsEmpty then
    SetError(ErrMemo, MainForm.Langs[eSshNoHost], Result);

  if (Port <= 0) or (Port > 65535) then
    SetError(ErrMemo, MainForm.Langs[eSshBadPort], Result);

  if Login.IsEmpty then
    SetError(ErrMemo, MainForm.Langs[eSshNoUser], Result);

  if AuthType = TAuthType.Key then
  begin
    if AuthText.IsEmpty then
      SetError(ErrMemo, MainForm.Langs[eSshNoKey], Result);

    if not FileExists(AuthText) then
      SetError(ErrMemo, MainForm.Langs[eSshBadKey], Result);
  end;

  if (LocalPort <= 0) or (LocalPort > 65535) then
    SetError(ErrMemo, MainForm.Langs[eSshBadLocalPort], Result);

  FileName := FPuTTYPath + 'plink.exe';
  if not FileExists(FileName) then
  begin
    SetError(ErrMemo, Format(MainForm.Langs[ePlinkNotFound], [FileName]), Dummy);
    SetError(ErrMemo, Format(MainForm.Langs[sPuTTYDwn], [sPuTTYUrl]), Result);
  end;

  FileName := FPuTTYPath + 'pageant.exe';
  if not FileExists(FileName) then
  begin
    SetError(ErrMemo, Format(MainForm.Langs[ePageantNotFound], [FileName]), Dummy);
    SetError(ErrMemo, Format(MainForm.Langs[sPuTTYDwn], [sPuTTYUrl]), Dummy);
  end;

  FChecked := True;
end;

procedure TSettingsForm.AuthTypeComboBoxChange(Sender: TObject);
var
  NeedClean: Boolean;
begin
  case AuthTypeComboBox.ItemIndex of
    AUTH_PAGEANT: begin
      NeedClean := (FDefaultConnection.AuthType <> TAuthType.Pageant) and
        (FDefaultConnection.AuthType <> TAuthType.Key);
      FDefaultConnection.AuthType := TAuthType.Pageant;
      AuthTextLabel.Caption := MainForm.Langs[sKeyPageant];
      AuthEdit.RightButton.ImageIndex := ICO_KEY;
      AuthEdit.RightButton.DisabledImageIndex := ICO_KEY;
      AuthEdit.RightButton.HotImageIndex := ICO_KEY;
      AuthEdit.RightButton.PressedImageIndex := ICO_KEY;
      AuthEdit.PasswordChar := #0;
    end;
    AUTH_KEY: begin
      NeedClean := (FDefaultConnection.AuthType <> TAuthType.Pageant) and
        (FDefaultConnection.AuthType <> TAuthType.Key);
      FDefaultConnection.AuthType := TAuthType.Key;
      AuthTextLabel.Caption := MainForm.Langs[sKeyPass];
      AuthEdit.RightButton.ImageIndex := ICO_KEY;
      AuthEdit.RightButton.DisabledImageIndex := ICO_KEY;
      AuthEdit.RightButton.HotImageIndex := ICO_KEY;
      AuthEdit.RightButton.PressedImageIndex := ICO_KEY;
      AuthEdit.PasswordChar := #0;
    end;
    AUTH_PASSWORD: begin
      NeedClean := FDefaultConnection.AuthType <> TAuthType.Password;
      FDefaultConnection.AuthType := TAuthType.Password;
      AuthTextLabel.Caption := MainForm.Langs[sPassword];
      AuthEdit.RightButton.ImageIndex := ICO_PASS_HIDDEN;
      AuthEdit.RightButton.DisabledImageIndex := ICO_PASS_HIDDEN;
      AuthEdit.RightButton.HotImageIndex := ICO_PASS_HIDDEN;
      AuthEdit.RightButton.PressedImageIndex := ICO_PASS_HIDDEN;
      AuthEdit.PasswordChar := PASSWORD_CHAR;
    end;
    else
      NeedClean := True;
  end;

  if NeedClean then
    AuthEdit.Text := '';
end;

procedure TSettingsForm.AuthEditRightButtonClick(Sender: TObject);
begin
  case FDefaultConnection.AuthType of
    TAuthType.Pageant, TAuthType.Key:
      if KeyFileDialog.Execute then
        AuthEdit.Text := KeyFileDialog.FileName;
    TAuthType.Password:
      if AuthEdit.RightButton.ImageIndex = ICO_PASS_HIDDEN then
      begin
        AuthEdit.RightButton.ImageIndex := ICO_PASS_SHOWED;
        AuthEdit.RightButton.DisabledImageIndex := ICO_PASS_SHOWED;
        AuthEdit.RightButton.HotImageIndex := ICO_PASS_SHOWED;
        AuthEdit.RightButton.PressedImageIndex := ICO_PASS_SHOWED;
        AuthEdit.PasswordChar := #0;
      end
      else begin
        AuthEdit.RightButton.ImageIndex := ICO_PASS_HIDDEN;
        AuthEdit.RightButton.DisabledImageIndex := ICO_PASS_HIDDEN;
        AuthEdit.RightButton.HotImageIndex := ICO_PASS_HIDDEN;
        AuthEdit.RightButton.PressedImageIndex := ICO_PASS_HIDDEN;
        AuthEdit.PasswordChar := PASSWORD_CHAR;
      end;
  end;
end;

procedure TSettingsForm.PuTTYEditRightButtonClick(Sender: TObject);
var
  PPath: string;
  PathSelected: Boolean;
begin
  PPath := ExcludeTrailingPathDelimiter(FPuTTYPath);
  if CheckWin32Version(6, 0) then
    with TFileOpenDialog.Create(nil) do
    try
      Title := MainForm.Langs[sSelectPuTTY];
      Options := [fdoPickFolders, fdoPathMustExist, fdoForceFileSystem];
      OkButtonLabel := MainForm.Langs[sPuTTYOK];
      DefaultFolder := PPath;
      FileName := PPath;
      PathSelected := Execute;
      if PathSelected then
        PPath := FileName;
    finally
      Free;
    end
  else
    PathSelected :=  SelectDirectory(MainForm.Langs[sSelectPuTTY],
      ExtractFileDrive(PPath), PPath, [sdNewUI, sdNewFolder]);

  if PathSelected then
  begin
    FPuTTYPath := IncludeTrailingPathDelimiter(PPath);
    PuTTYEdit.Text := ExcludeTrailingPathDelimiter(PPath);
  end;
end;

initialization
  CryptoLib := LoadLibrary(PChar(crypt32));
  Win32Check(CryptoLib <> 0);
  CryptProtectMemory := GetProcAddress(CryptoLib, PChar('CryptProtectMemory'));
  CryptUnprotectMemory := GetProcAddress(CryptoLib, PChar('CryptUnprotectMemory'));
  if not Assigned(CryptProtectMemory) or not Assigned(CryptUnprotectMemory) then
  begin
    { Слишком старая Windows }
    CryptProtectMemory := nil;
    CryptUnprotectMemory := nil;
    FreeLibrary(CryptoLib);

    CryptoLib := LoadLibrary(PChar(advapi32legacy));
    Win32Check(CryptoLib <> 0);
    RtlEncryptMemory := GetProcAddress(CryptoLib, PChar('SystemFunction040'));
    Win32Check(Assigned(RtlEncryptMemory));
    RtlDecryptMemory := GetProcAddress(CryptoLib, PChar('SystemFunction041'));
    Win32Check(Assigned(RtlEncryptMemory));
  end;

finalization
  CryptProtectMemory := nil;
  CryptUnprotectMemory := nil;
  RtlEncryptMemory := nil;
  RtlDecryptMemory := nil;
  if CryptoLib <> 0 then
    FreeLibrary(CryptoLib);
end.
