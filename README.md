﻿# SshTun
SshTun это небольшая программа, позволяющая вам использовать практически любой сервер, к которому у вас есть доступ по ssh в качестве socks5 прокси сервера.

# SshTun
SshTun is a small program that allows you to use almost any server to which you have ssh access as a socks5 proxy server.
