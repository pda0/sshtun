(******************************************************************************
 *  SshTun, is a small utility that allows you to turn almost any ssh server  *
 *  into a proxy server.                                                      *
 *  Copyright (C) 2018  Dmitriy Pomerantsev                                   *
 *                                                                            *
 *  This program is free software: you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published by      *
 *  the Free Software Foundation, either version 3 of the License, or         *
 *  (at your option) any later version.                                       *
 *                                                                            *
 *  This program is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *  GNU General Public License for more details.                              *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.    *
 ******************************************************************************)
unit Langs;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Winapi.Windows, System.Classes, Vcl.Forms, System.Generics.Collections;

type
  TLangs = class
  private type
    TFormCallback = reference to procedure(Target: TComponent; ResId, SrcValue:
      string; out DestValue: string);
    TFormInfo = record
      Form: TForm;
      BeforeEvent, AfterEvent: TNotifyEvent;
    end;
  private
    FForms: TList<TLangs.TFormInfo>;
    FOrigStrings, FTransStrings: TDictionary<string, string>;
    FWinBuffer: array of Char;
    FCurrentLang: LCID;
    procedure SetLang(Value: LCID);
    function GetT(Value: string): string;
    procedure ResetLang;
    procedure ReloadLang;
    procedure WalkForm(const AForm: TForm; CallBack: TFormCallback);
  public const
    LANG_ENU: LCID = $409;
  public
    constructor Create(DefaultLang: LCID);
    destructor Destroy; override;
    function LocaleInfo(Locale: LCID; LCType: LCTYPE): string;
    procedure RegisterForm(const AForm: TForm; BeforeUpdate, AfterUpdate:
      TNotifyEvent);
    property T[Value: string]: string read GetT; default;
    property Lang: LCID read FCurrentLang write SetLang;
  end;

implementation

uses
  System.SysUtils, Vcl.Controls, Vcl.StdCtrls, Vcl.ComCtrls, System.Actions,
  Vcl.ActnList, System.IniFiles, Dialogs;

type
  TOpenControl = class(TControl)
  public
    property Caption;
  end;

{ TLangs }

constructor TLangs.Create(DefaultLang: LCID);
begin
  FCurrentLang := DefaultLang;
  FForms := TList<TFormInfo>.Create;
  FOrigStrings := TDictionary<string, string>.Create;
  FTransStrings := TDictionary<string, string>.Create;
  SetLength(FWinBuffer, MAX_PATH);
end;

destructor TLangs.Destroy;
begin
  FreeAndNil(FForms);
  FreeAndNil(FOrigStrings);
  FreeAndNil(FTransStrings);

  inherited;
end;

procedure TLangs.SetLang(Value: LCID);
var
  i: Integer;
begin
  if FCurrentLang <> Value then
  begin
    for i := 0 to FForms.Count - 1 do
      if Assigned(FForms[i].BeforeEvent) then
        FForms[i].BeforeEvent(Self);
    try
      FCurrentLang := Value;
      ReloadLang;
    finally
      for i := 0 to FForms.Count - 1 do
        if Assigned(FForms[i].AfterEvent) then
          FForms[i].AfterEvent(Self);
    end;
  end;
end;

function TLangs.GetT(Value: string): string;
begin
  if not FTransStrings.TryGetValue(Value, Result) then
    Result := Value;
end;

procedure TLangs.ResetLang;
var
  FormName: string;
  i: Integer;
begin
  { ���������� ������������� ������ �� ������ }
  FTransStrings.Clear;

  for i := 0 to FForms.Count - 1 do
  begin
    FormName := FForms[i].Form.Name;
    WalkForm(FForms[i].Form, procedure(Target: TComponent; ResId, SrcValue:
      string; out DestValue: string)
    begin
      if ResId = 'MainForm.Caption' then
        DestValue := SrcValue
      else
        if not FOrigStrings.TryGetValue(ResId, DestValue) then
          DestValue := SrcValue;
    end);
  end;
end;

procedure TLangs.ReloadLang;
var
  LangStrings: TMemIniFile;
  SectionData: TStrings;
  FileName, FormName, Key, TransText: string;
  Locale: LCID;
  i: Integer;
begin
  if FCurrentLang <> 0 then
    Locale := FCurrentLang
  else
    Locale := GetUserDefaultLCID;

  FTransStrings.Clear;
  if Locale <> TLangs.LANG_ENU then
  begin
    {$IFDEF DEBUG}
    FileName := '..\..\' + ExtractFileName(Application.ExeName);
    {$ELSE}
    FileName := Application.ExeName;
    {$ENDIF}
    FileName := ChangeFileExt(FileName, '.' + LocaleInfo(Locale, LOCALE_SABBREVLANGNAME).ToLowerInvariant);

    LangStrings := nil;
    SectionData := nil;
    if FileExists(FileName) then
    try
      SectionData := TStringList.Create;
      LangStrings := TMemIniFile.Create(FileName, TEncoding.UTF8, True);

      for i := 0 to FForms.Count - 1 do
      begin
        FormName := FForms[i].Form.Name;
        WalkForm(FForms[i].Form, procedure(Target: TComponent; ResId, SrcValue:
          string; out DestValue: string)
        var
          i: Integer;
        begin
          if ResId = 'MainForm.Caption' then
            DestValue := SrcValue
          else begin
            if (Target is TComboBox) or (Target is TFileOpenDialog) then
            begin
              SectionData.Clear;
              i := 0;
              while True do
              begin
                DestValue := LangStrings.ReadString(FormName, ResId + '[' + i.ToString + ']', '');
                if DestValue.IsEmpty then
                  Break
                else
                  SectionData.Add(DestValue);

                Inc(i);
              end;

              if SectionData.Text.IsEmpty then
                DestValue := SrcValue
              else
                DestValue := SectionData.Text;
            end
            else begin
              FOrigStrings.TryGetValue(ResId, SrcValue);
              DestValue := LangStrings.ReadString(FormName, ResId, SrcValue);
            end;
          end;
        end);
      end;

      LangStrings.ReadSection('Strings', SectionData);
      for Key in SectionData do
      begin
        TransText := LangStrings.ReadString('Strings', Key, '');
        if not TransText.IsEmpty then
          FTransStrings.AddOrSetValue(Key, TransText);
      end;
    finally
      FreeAndNil(SectionData);
      FreeAndNil(LangStrings);
    end
    else begin
      { � ����� ������-�� ���... ���������� �� default }
      FCurrentLang := 0;
      ResetLang;
    end;
  end
  else
    ResetLang;
end;

procedure TLangs.WalkForm(const AForm: TForm; CallBack: TFormCallback);
var
  DestValue: string;
  i: Integer;

  procedure ProcessComponent(const AComp: TComponent; BaseName: string);
  var
    TmpList: TStrings;
    TmpStr: string;
    i: Integer;
  begin
    if AComp is TComboBox then
    begin
      CallBack(AComp, BaseName + AComp.Name + '.Items', (AComp as TComboBox).Items.Text, DestValue);
      if (AComp as TComboBox).Items.Text <> DestValue then
        (AComp as TComboBox).Items.Text := DestValue;
    end
    else if AComp is TContainedAction then
    begin
      CallBack(AComp, BaseName + AComp.Name + '.Caption', (AComp as TContainedAction).Caption, DestValue);
      if (AComp as TContainedAction).Caption <> DestValue then
        (AComp as TContainedAction).Caption := DestValue;
    end
    else if AComp is TFileOpenDialog then
    begin
      TmpList := TStringList.Create;
      try
        for i := 0 to (AComp as TFileOpenDialog).FileTypes.Count - 1 do
          TmpList.Add((AComp as TFileOpenDialog).FileTypes.Items[i].DisplayName);

        CallBack(AComp, BaseName + AComp.Name + '.FileTypes', TmpList.Text, TmpStr);
        TmpList.Text := TmpStr;
        if TmpList.Count >= (AComp as TFileOpenDialog).FileTypes.Count then
          for i := 0 to (AComp as TFileOpenDialog).FileTypes.Count - 1 do
            (AComp as TFileOpenDialog).FileTypes.Items[i].DisplayName := TmpList[i];
      finally
        FreeAndNil(TmpList);
      end;
    end
    else if AComp is TControl then
    begin
      CallBack(AComp, BaseName + AComp.Name + '.Caption', TOpenControl(AComp).Caption, DestValue);
      if TOpenControl(AComp).Caption <> DestValue then
        TOpenControl(AComp).Caption := DestValue;
    end;
  end;
begin
  ProcessComponent(AForm, string.Empty);
  for i := 0 to AForm.ComponentCount - 1 do
    ProcessComponent(AForm.Components[i], AForm.Name + '.');
end;

function TLangs.LocaleInfo(Locale: LCID; LCType: LCTYPE): string;
var
  StrLen: DWORD;
begin
  StrLen := GetLocaleInfo(Locale, LCType, nil, 0);
  Win32Check(StrLen > 0);
  if Cardinal(Length(FWinBuffer)) < StrLen then
    SetLength(FWinBuffer, StrLen);
  StrLen := GetLocaleInfo(Locale, LCType, @FWinBuffer[0], Length(FWinBuffer));
  Win32Check(StrLen > 0);

  Result := string.Create(FWinBuffer, 0, StrLen - 1);
end;

procedure TLangs.RegisterForm(const AForm: TForm; BeforeUpdate, AfterUpdate:
  TNotifyEvent);
var
  FormInfo: TLangs.TFormInfo;
begin
  FormInfo.Form := AForm;
  FormInfo.BeforeEvent := BeforeUpdate;
  FormInfo.AfterEvent := AfterUpdate;

  WalkForm(AForm, procedure(Target: TComponent; ResId, SrcValue: string;
    out DestValue: string)
  begin
    DestValue := SrcValue;
    FOrigStrings.AddOrSetValue(ResId, SrcValue);
  end);

  FForms.Add(FormInfo);
end;

end.
